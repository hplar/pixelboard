#Makefile

.PHONY: venv venv/contracts venv/messenger

.ONESHELL: venv/contracts venv/messenger

CONTRACTS_PATH=./px-contracts
MESSENGER_PATH=./px-messenger


# PYTHON SETUP

PYTHON_VERSION=3.9.10

CONTRACTS_VENV_PATH=${CONTRACTS_PATH}/.venv
MESSENGER_VENV_PATH=${MESSENGER_PATH}/.venv

symlink/factory_address:
	ln -s ./px-contracts/.env.blockchain_addresses
	ln -s .env.blockchain_addresses ./px-messenger/.env.blockchain_addresses

venv/contracts:
	cd ${CONTRACTS_PATH}
	poetry install -qn
	@echo "Created px-contracts virtualenv"

venv/messenger:
	cd ${MESSENGER_PATH}
	poetry install -qn
	@echo "Created px-messenger virtualenv"

venv: venv/contracts venv/messenger
	@echo "done"


# DOCKER-COMPOSE

docker/up:
	docker-compose up -d

docker/down:
	docker-compose down

docker/kill:
	docker-compose kill || echo "nothing to kill"


# CLEANUP

clean/all: clean/docker clean/venv

clean/docker: docker/down
	docker-compose rm
	docker volume rm pixelboard_zookeeper_data
	docker volume rm pixelboard_kafka_data
	docker volume rm pixelboard_vechain_data

clean/redis:
	redis-cli flushall

clean/venv:
	rm -rf ${CONTRACTS_VENV_PATH}
	rm -rf ${MESSENGER_VENV_PATH}

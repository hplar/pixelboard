#!/usr/bin/env python3

import argparse
import json
import random

from addrs import PB_ADDRS
from config import CONTRACTS, connector, wallet


mnemonic = [
    "denial",
    "kitchen",
    "pet",
    "squirrel",
    "other",
    "broom",
    "bar",
    "gas",
    "better",
    "priority",
    "spoil",
    "cross",
]

size = connector.call(wallet.address, CONTRACTS["pixelboard"], "size", [], PB_ADDRS[0])


def driver(ids: list[int], color: str, randomize: bool):
    for id in ids:
        try:
            if randomize:
                r = lambda: random.randint(0, 255)
                color = "%02X%02X%02X" % (r(), r(), r())

            resp = connector.transact(
                wallet,
                CONTRACTS["pixelboard"],
                "updatePixel",
                [id, color.encode()],
                PB_ADDRS[0],
            )

            print(resp["id"])
        except Exception as exc:
            print(exc)
            continue


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--color", help="Color hex value")
    parser.add_argument(
        "--random", help="Use a random color", action="store_true", default=False
    )
    parser.add_argument(
        "--ids", help="Pixel ids as separate arguments", nargs="*", type=int
    )

    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()
    driver(args.ids, args.color, args.random)

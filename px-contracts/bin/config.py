from thor_requests.connect import Connect
from thor_requests.contract import Contract
from thor_requests.wallet import Wallet

THOR_URL = "http://localhost:8669"


CONTRACTS = {
    "factory": Contract.fromFile("build/contracts/PixelboardFactory.json"),
    "pixelboard": Contract.fromFile("build/contracts/Pixelboard.json"),
    # "nft": Contract.fromFile("../brownie/build/contracts/TokenFactory.json"),
}

# denial kitchen pet squirrel other broom bar gas better priority spoil cross

mnemonic = [
    "denial",
    "kitchen",
    "pet",
    "squirrel",
    "other",
    "broom",
    "bar",
    "gas",
    "better",
    "priority",
    "spoil",
    "cross",
]

wallet = Wallet.fromMnemonic(mnemonic)
connector = Connect(THOR_URL)

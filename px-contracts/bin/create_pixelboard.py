#!/usr/bin/env python3

import random

from addrs import CONTRACTS, FACT_ADDR
from config import connector, wallet


board_count = connector.call(
    wallet.address, CONTRACTS["factory"], "boardCount", [], FACT_ADDR
)["decoded"]["0"]

amount = random.randint(4, 16)

resp = connector.transact(
    wallet,
    CONTRACTS["factory"],
    "createPixelboard",
    [board_count + 1, amount, amount, f"Test Board".encode()],
    FACT_ADDR,
)

print(resp)

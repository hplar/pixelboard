#!/usr/bin/env python3

import logging
import random

from config import CONTRACTS, connector, wallet


log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler())
log.setLevel(logging.DEBUG)


def deploy_factory(f):
    log.debug("[*] Deploying factory contract")

    resp = connector.deploy(wallet, CONTRACTS["factory"])
    recpt = connector.wait_for_tx_receipt(resp["id"])
    factory_address = recpt["outputs"][0]["contractAddress"]

    return factory_address, recpt


def deploy_pixelboards(f, factory_address):
    log.debug("[*] Deploying pixelboards")

    pixelboards = []

    log.debug("[*] Deploying Partial Pixelboard")
    resp = connector.transact(
        wallet,
        CONTRACTS["factory"],
        "createPixelboard",
        [0, 5, 5, f"Partial".encode()],
        factory_address,
    )
    recpt = connector.wait_for_tx_receipt(resp["id"])
    address = recpt["outputs"][0]["events"][0]["address"]

    pixelboards.append(
        {
            "title": "Partial",
            "address": address,
            "gasUsed": recpt["gasUsed"],
        }
    )

    return pixelboards


# def deploy_nftcontract(f):
#     log.debug("[*] Deploying NFT contract")
#
#     resp = connector.deploy(
#         wallet, CONTRACTS["nft"], ["string", "string"], ["Pixelboard", "PB"]
#     )
#     recpt = connector.wait_for_tx_receipt(resp["id"])
#     nft_address = recpt["outputs"][0]["contractAddress"]
#
#     return nft_address, recpt


def supply_pixels(f, pixelboards):
    log.debug("[*] Supplying pixels")

    tx_ids = []
    for pb in pixelboards:
        if "Partial" in pb["title"]:
            log.debug("[*] Supplying pixels to Partial Board")

            qtyPixels = connector.call(
                wallet.address, CONTRACTS["pixelboard"], "qtyPixels", [], pb["address"]
            )

            count = 0

            for i in range(16):
                addr_current_price = connector.call(
                    wallet.address,
                    CONTRACTS["pixelboard"],
                    "addrCurrentPrice",
                    [wallet.address],
                    pb["address"],
                )["decoded"]["0"]

                randomPixelId = random.randint(1, qtyPixels["decoded"]["0"] - 1)

                r = lambda: random.randint(0, 255)
                color = "%02X%02X%02X" % (r(), r(), r())

                try:
                    tx_id = connector.transact(
                        wallet,
                        CONTRACTS["pixelboard"],
                        "buyPixel",
                        [randomPixelId, color.encode()],
                        pb["address"],
                        # 5e18.as_integer_ratio()[0] - i * 1e18 .as_integer_ratio()[0],
                        5e18 .as_integer_ratio()[0],
                    )["id"]

                    connector.wait_for_tx_receipt(tx_id)
                    count += 1
                except Exception as exc:
                    print(exc)
                    continue

            print(f"[*] Pixels supplied: {count}")
            print(f"[*] Last pixel price: {addr_current_price.as_integer_ratio()[0]}")

    return tx_ids


def get_pixelboards_info(f, pixelboards):
    log.debug("[*] Getting pixelboards info")

    for pb in pixelboards:
        pb["title"] = connector.call(
            wallet.address,
            CONTRACTS["pixelboard"],
            "title",
            [],
            pb["address"],
        )["decoded"]["0"]

        pb["qtyPixels"] = connector.call(
            wallet.address,
            CONTRACTS["pixelboard"],
            "qtyPixels",
            [],
            pb["address"],
        )["decoded"]["0"]

        pb["width"] = connector.call(
            wallet.address,
            CONTRACTS["pixelboard"],
            "width",
            [],
            pb["address"],
        )["decoded"]["0"]

        pb["height"] = connector.call(
            wallet.address,
            CONTRACTS["pixelboard"],
            "height",
            [],
            pb["address"],
        )["decoded"]["0"]

    return pixelboards


def get_factory_info(f, factory_address):
    log.debug("[*] Getting factory info")

    board_count = connector.call(
        wallet.address,
        CONTRACTS["factory"],
        "boardCount",
        [],
        factory_address,
    )["decoded"]["0"]

    addresses = connector.call(
        wallet.address,
        CONTRACTS["factory"],
        "getAddresses",
        [],
        factory_address,
    )["decoded"]["0"]

    return {
        "address": factory_address,
        "board_count": board_count,
        "addresses": addresses,
    }


def get_nft_info(f, nft_address):
    log.debug("[*] Getting NFT  info")

    return {
        "address": nft_address,
    }


def print_info(f, factory, pixelboards):
    log.debug("[*] Info")
    log.debug("  Factory:")

    for k, v in factory.items():
        if type(v) == tuple:
            log.debug("    Addresses:")
            for addr in v:
                log.debug(f"    {addr}")
            continue
        else:
            log.debug(f"  {k}: {v}")
    log.debug("")

    log.debug("  Pixelboards:")
    for pixelboard in pixelboards:
        for k, v in pixelboard.items():
            log.debug(f"    {k}: {v}")
        log.debug("")

    # log.debug("  NFT:")
    # log.debug("    Address: " + nft["address"] + "")


def print_gas_used(fact_tx_rcpt, pixelboards, tx_ids):
    log.debug("[*] Figuring out gas used and prices")

    vtho_price = 0.00807
    log.debug(f"  VTHO price:   {vtho_price}")

    fact_gas_used = fact_tx_rcpt["gasUsed"]
    fact_vtho_used = fact_gas_used / 1000
    fact_usd_used = fact_vtho_used * vtho_price

    log.debug("")
    log.debug("  Factory:")
    log.debug(f"    Gas used:     {fact_gas_used}")
    log.debug(f"    USD used:     {fact_usd_used}")
    log.debug("")

    log.debug("  Pixelboards:")
    for pb in pixelboards:
        title = pb["title"].decode()
        pb_gas_used = pb["gasUsed"]
        pb_vtho_used = pb_gas_used / 1000
        pb_usd_used = pb_vtho_used * vtho_price

        log.debug(f"    Title:        {title}")
        log.debug(f"      Gas used:     {pb_gas_used}")
        log.debug(f"      VTHO used:     {pb_vtho_used}")
        log.debug(f"      USD used:     {pb_usd_used}")
        log.debug("")

    log.debug("  Pixel supply:")
    pixel_supply_total_gas_used = 0
    for id in tx_ids:
        tx = connector.wait_for_tx_receipt(id, timeout=20)
        if tx:
            pixel_supply_total_gas_used += tx["gasUsed"]

    pb_vtho_used = pixel_supply_total_gas_used / 1000
    pb_usd_used = pb_vtho_used * vtho_price

    log.debug(f"    Gas used:     {pixel_supply_total_gas_used}")
    log.debug(f"    VTHO used:    {pb_vtho_used}")
    log.debug(f"    USD used:     {pb_usd_used}")


def write_env_file(factory, pixelboards):
    with open("./addrs.py", "w") as f:
        f.write(f"FACT_ADDR=\"{factory['address']}\"\n")
        f.write("PB_ADDRS=[\n")
        for pb in pixelboards:
            f.write(f"    \"{pb['address']}\",\n")
        f.write("]")


def driver():
    with open("./report.txt", "w") as f:
        factory_address, fact_tx_rcpt = deploy_factory(f)
        pixelboards = deploy_pixelboards(f, factory_address)
        # nft_address, nft_tx_rcpt = deploy_nftcontract(f)
        tx_ids = supply_pixels(f, pixelboards)
        pixelboards = get_pixelboards_info(f, pixelboards)
        factory = get_factory_info(f, factory_address)
        # nft = get_nft_info(f, nft_address)
        print_info(f, factory, pixelboards)
        print_gas_used(fact_tx_rcpt, pixelboards, tx_ids)

    write_env_file(factory, pixelboards)


if __name__ == "__main__":
    driver()

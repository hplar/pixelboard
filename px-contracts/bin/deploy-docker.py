#!/usr/bin/env python3

import argparse
import logging
import random
import time
from typing import List, Union

import requests
from thor_requests.connect import Connect
from thor_requests.contract import Contract
from thor_requests.wallet import Wallet

from config import CONTRACTS, connector, wallet

log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler())
log.setLevel(logging.DEBUG)


VTHO_PRICE = float(
    requests.get(
        "https://www.binance.com/api/v3/ticker/price", params={"symbol": "VTHOUSDT"}
    ).json()["price"]
)


class Factory:
    def __init__(self) -> None:
        self.connector: Connect = connector
        self.contract: Contract = CONTRACTS["factory"]
        self.wallet: Wallet = wallet
        self.address: str = ""
        self.pixelboards: List[Pixelboard] = []
        self.receipt: Union[dict, None] = None
        self.deploy_duration: float = 0
        self.deploy_gas: int = 0
        self.deploy_vtho: float = 0
        self.deploy_usd: float = 0

    def deploy(self) -> None:
        log.debug("[*] Deploying factory contract")
        start: float = time.time()

        resp: dict = connector.deploy(wallet, CONTRACTS["factory"])
        receipt: dict = connector.wait_for_tx_receipt(resp["id"])
        self.address = receipt["outputs"][0]["contractAddress"]

        end: float = time.time()
        self.deploy_duration: float = end - start
        self.receipt = receipt

        log.debug(f"[*] Deployed factory at: {self.address}")

    def deploy_pixelboard(self, title: str, width: int, height: int) -> None:
        log.debug("[*] Deploying pixelboard contract")
        id = len(self.pixelboards)

        start: float = time.time()
        transaction = self.connector.transact(
            self.wallet,
            self.contract,
            "createPixelboard",
            [id, width, height, title.encode()],
            self.address,
        )
        receipt: dict = connector.wait_for_tx_receipt(transaction["id"])
        address: str = receipt["outputs"][0]["events"][0]["address"]
        end: float = time.time()

        deploy_duration: float = end - start

        pixelboard: Pixelboard = Pixelboard(
            id, address, title, width, height, deploy_duration, receipt
        )
        self.pixelboards.append(pixelboard)

    def gather_stats(self):
        self.deploy_gas = self.receipt["gasUsed"]
        self.deploy_vtho = self.deploy_gas / 1000
        self.deploy_usd = self.deploy_vtho * VTHO_PRICE

    def log(self) -> None:
        log.debug("FACTORY INFO")
        log.debug("---------------------------------------------")
        log.debug(f"address     :   {self.address}")
        log.debug(f"wallet      :   {self.wallet.address}")
        log.debug(f"boards      :   {len(self.pixelboards)}")
        log.debug(f"deploy gas  :   {self.deploy_gas}")
        log.debug(f"deploy vtho :   {self.deploy_vtho}")
        log.debug(f"deploy usd  :   {self.deploy_usd}")
        log.debug(f"deploy time :   {self.deploy_duration}")
        log.debug("")


class Pixelboard:
    def __init__(
        self,
        id: int,
        address: str,
        title: str,
        width: int,
        height: int,
        deploy_duration: float,
        receipt: dict,
    ) -> None:
        self.id = id
        self.address = address
        self.title = title
        self.width = width
        self.height = height
        self.size = width * height
        self.connector: Connect = connector
        self.contract: Contract = CONTRACTS["pixelboard"]
        self.wallet: Wallet = wallet
        self.transactions: List[dict] = []
        self.receipt = receipt
        self.deploy_duration = deploy_duration
        self.deploy_gas: int = receipt["gasUsed"]
        self.deploy_vtho: float = self.deploy_gas / 1000
        self.deploy_usd: float = self.deploy_vtho * VTHO_PRICE
        self.pixels_receipts: List[dict] = []
        self.pixels_duration: float = 0
        self.pixels_gas: int = 0
        self.pixels_vtho: float = 0
        self.pixels_usd: float = 0

    @staticmethod
    def random_color_hex() -> str:
        red = random.randint(0, 255)
        green = random.randint(0, 255)
        blue = random.randint(0, 255)

        return "%02X%02X%02X" % (red, green, blue)

    def random_pixel_id(self) -> int:
        return random.randint(1, self.size - 1)

    def supply_pixel(self) -> None:
        log.debug(f"[*] Supplying pixel to: {self.title}")
        pixel_id: int = self.random_pixel_id()
        pixel_color: str = self.random_color_hex()

        start: float = time.time()
        # noinspection PyBroadException
        try:
            transaction: dict = connector.transact(
                self.wallet,
                self.contract,
                "buyPixel",
                [pixel_id, pixel_color.encode()],
                self.address,
                5e18 .as_integer_ratio()[0],
            )

            receipt: dict = connector.wait_for_tx_receipt(transaction["id"])
            self.transactions.append(transaction)
            self.pixels_receipts.append(receipt)
        except Exception as exc:
            log.warning(exc)

        end: float = time.time()
        self.pixels_duration += end - start

    def gather_stats(self):
        for receipt in self.pixels_receipts:
            self.pixels_gas += receipt["gasUsed"]

        self.pixels_vtho = self.pixels_gas / 1000
        self.pixels_usd = self.pixels_vtho * VTHO_PRICE

    def log(self) -> None:
        log.debug(f"{self.title.upper()} INFO")
        log.debug("---------------------------------------------")
        log.debug(f"id          :   {self.id}")
        log.debug(f"title:      :   {self.title}")
        log.debug(f"address     :   {self.address}")
        log.debug(f"width       :   {self.width}")
        log.debug(f"height      :   {self.height}")
        log.debug(f"deploy gas  :   {self.deploy_gas}")
        log.debug(f"deploy vtho :   {self.deploy_vtho}")
        log.debug(f"deploy usd  :   {self.deploy_usd}")
        log.debug(f"deploy time :   {self.deploy_duration}")
        log.debug(f"pixels      :   {len(self.pixels_receipts)}")
        log.debug(f"pixels gas  :   {self.pixels_gas}")
        log.debug(f"pixels vtho :   {self.pixels_vtho}")
        log.debug(f"pixels usd  :   {self.pixels_usd}")
        log.debug(f"pixels time :   {self.pixels_duration}")
        log.debug("")


class LogHandler:
    def __init__(self, factory: Factory):
        self.factory = factory
        self.pixelboards: List[Pixelboard] = factory.pixelboards
        self.total_pixels_supplied: int = 0
        self.total_gas: int = 0
        self.total_vtho: float = 0
        self.total_usd: float = 0
        self.total_duration: float = 0

    def gather_stats(self):
        self.factory.gather_stats()

        for pixelboard in self.pixelboards:
            pixelboard.gather_stats()

    def parse(self):
        self.total_duration += self.factory.deploy_duration
        self.total_gas += self.factory.deploy_gas

        for pixelboard in self.pixelboards:
            self.total_gas += pixelboard.deploy_gas
            self.total_duration += pixelboard.deploy_duration
            self.total_duration += pixelboard.pixels_duration
            self.total_pixels_supplied += len(pixelboard.pixels_receipts)
            for receipt in pixelboard.pixels_receipts:
                self.total_gas += receipt["gasUsed"]

        self.total_vtho = self.total_gas / 1000
        self.total_usd = self.total_vtho * VTHO_PRICE

    def log(self):
        self.gather_stats()
        self.parse()

        self.factory.log()
        for pixelboard in self.pixelboards:
            pixelboard.log()

        log.debug("SUMMARY")
        log.debug("---------------------------------------------")
        log.debug(f"boards : {len(self.pixelboards)}")
        log.debug(f"pixels : {self.total_pixels_supplied}")
        log.debug(f"gas    : {self.total_gas}")
        log.debug(f"vtho   : {self.total_vtho}")
        log.debug(f"usd    : {self.total_usd}")
        log.debug(f"time   : {self.total_duration}")


class DotEnvWriter:
    def __init__(self, factory: Factory) -> None:
        self.factory = factory
        self.path = '.env.blockchain_addresses'

    def write(self) -> None:
        log.info("[*] Updating dotenv file")
        factory_address = f"FACT_ADDR={self.factory.address}"
        with open(self.path, 'w') as env_file:
            env_file.write(factory_address)


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser("Deploy Pixelboard related contracts")
    parser.add_argument(
        "pixelboards", metavar="N", type=int, help="Number of pixelboards to deploy"
    )
    parser.add_argument(
        "width", metavar="W", type=int, help="Number of columns per pixelboard"
    )
    parser.add_argument(
        "height", metavar="H", type=int, help="Number of rows per pixelboard"
    )
    parser.add_argument(
        "--write-dotenv", action="store_true", help="Write factory address to dotenv file"
    )
    parser.add_argument(
        "--with-pixels",
        action="store_true",
        help="Supply pixelboards with random pixels",
    )

    return parser.parse_args()


def logger(factory) -> None:
    log.debug("")
    factory.log()
    for pixelboard in factory.pixelboards:
        pixelboard.log()


def driver():
    args = parse_args()

    factory: Factory = Factory()
    factory.deploy()

    width = args.width if args.width else 6
    height = args.height if args.height else 6

    for i in range(0, args.pixelboards):
        factory.deploy_pixelboard(f"Pixelboard{i}", width=width, height=height)

    if args.with_pixels:
        for pixelboard in factory.pixelboards:
            for i in range(0, pixelboard.size // 10):
                pixelboard.supply_pixel()

    if args.write_dotenv:
        writer: DotEnvWriter = DotEnvWriter(factory)
        writer.write()

    LogHandler(factory).log()


if __name__ == "__main__":
    driver()

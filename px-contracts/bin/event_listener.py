#!/usr/bin/env python3

import json
from time import sleep

from pymongo.errors import DuplicateKeyError
import requests

from addrs import PB_ADDRS
from config import CONTRACTS, connector, db


contract = CONTRACTS["pixelboard"]
px_updated_topic = contract.get_events()[0].get_signature()


def decode_event(event):
    event_obj = contract.get_event_by_signature(bytes.fromhex(event["topics"][0][2:]))
    decoded = event_obj.decode(
        bytes.fromhex(event["data"][2:]),
        [bytes.fromhex(topic[2:]) for topic in event["topics"]],
    )

    return decoded


total = 0

events_seen = {PB_ADDR: set() for PB_ADDR in PB_ADDRS}

while True:
    print("Looking for events on all pixelboards")

    for PB_ADDR in PB_ADDRS:
        count = 0

        while True:
            best_block = connector.get_block("best")["number"]

            data = {
                "range": {"unit": "block", "from": 0, "to": best_block},
                "options": {"offset": count, "limit": 1},
                "criteriaSet": [
                    {
                        "address": PB_ADDR,
                        "topic0": px_updated_topic.hex(),
                    }
                ],
                "order": "asc",
            }

            resp = requests.post("http://localhost:8669/logs/event", json=data)
            j = json.loads(resp.content)

            if j == []:
                break

            for a in j:
                decoded = decode_event(a)

                doc = {
                    "id": decoded["index"],
                    "owner": decoded["owner"],
                    "color": "#" + decoded["color"].rstrip(b"\x00").decode(),
                }

                if doc["id"] in events_seen[PB_ADDR]:
                    # print(f"Event {doc['id']} skipped : event already processed")
                    count += 1
                    continue

                try:
                    selector = {id: 0}
                    coll = db[PB_ADDR]
                    coll.update(
                        {},
                        {"$addToSet": {"pixels": doc}},
                        upsert=True,
                    )
                    print("id: ", decoded["index"])
                    print("owner: ", decoded["owner"])
                    print("color: ", decoded["color"].rstrip(b"\x00").decode())
                    print("")
                    events_seen[PB_ADDR].add(decoded["index"])
                    total += 1
                except DuplicateKeyError:
                    print("ignoring duplicate key")
                count += 1

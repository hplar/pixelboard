#!/usr/bin/env python3

from config import CONTRACTS, connector, wallet
from addrs import PB_ADDRS

addr = PB_ADDRS[0]

size = connector.call(wallet.address, CONTRACTS["pixelboard"], "size", [], addr)

filled = connector.call(wallet.address, CONTRACTS["pixelboard"], "filled", [], addr)

# totalSold = connector.call(
#     wallet.address, CONTRACTS["pixelboard"], "totalSold", [], addr
# )

# vetReceived = connector.call(
#     wallet.address, CONTRACTS["pixelboard"], "vetReceived", [], addr
# )

# share_1 = connector.call(
#     wallet.address,
#     CONTRACTS["pixelboard"],
#     "getShare",
#     ["0xf077b491b355e64048ce21e3a6fc4751eeea77fa"],
#     addr,
# )
#
# share_2 = connector.call(
#     wallet.address,
#     CONTRACTS["pixelboard"],
#     "getShare",
#     ["0x435933c8064b4ae76be665428e0307ef2ccfbd68"],
#     addr,
# )


print("size: ", size["decoded"]["0"])
# print("totalSold: ", totalSold["decoded"]["0"])
print("filled: ", filled["decoded"]["0"])
# print("VETreceived: ", vetReceived["decoded"]["0"] / 1e18)
# print(
#     "share (0xf077b491b355e64048ce21e3a6fc4751eeea77fa): ",
#     share_1["decoded"]["0"] / 1e18,
# )
# print(
#     "share (0x435933c8064b4ae76be665428e0307ef2ccfbd68): ",
#     share_2["decoded"]["0"] / 1e18,
# )

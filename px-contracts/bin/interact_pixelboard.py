#!/usr/bin/env python3

from config import CONTRACTS, connector, wallet
from addrs import PB_ADDRS

for addr in PB_ADDRS:
    title = connector.call(wallet.address, CONTRACTS["pixelboard"], "title", [], addr)
    width = connector.call(wallet.address, CONTRACTS["pixelboard"], "width", [], addr)
    height = connector.call(wallet.address, CONTRACTS["pixelboard"], "height", [], addr)
    completed = connector.call(
        wallet.address, CONTRACTS["pixelboard"], "completed", [], addr
    )
    addrCurrentPrice = connector.call(
        wallet.address,
        CONTRACTS["pixelboard"],
        "addrCurrentPrice",
        [wallet.address],
        addr,
    )
    qtySold = connector.call(
        wallet.address, CONTRACTS["pixelboard"], "qtySold", [], addr
    )
    qtyVet = connector.call(wallet.address, CONTRACTS["pixelboard"], "qtyVet", [], addr)

    pixel_sold_topic = CONTRACTS["pixelboard"].get_events()[1].get_signature().hex()
    value_received_topic = CONTRACTS["pixelboard"].get_events()[2].get_signature().hex()

    print("address: ", addr)
    print("title: ", title["decoded"]["0"])
    print("width: ", width["decoded"]["0"])
    print("height: ", height["decoded"]["0"])
    print("completed: ", completed["decoded"]["0"])
    print("addrCurrentPrice: ", addrCurrentPrice["decoded"]["0"])
    print("qtySold: ", qtySold["decoded"]["0"])
    print("qtyVet: ", qtyVet["decoded"]["0"])
    print("pixel_sold_topic_sig: ", pixel_sold_topic)
    print("value_recvied_topic_sig: ", value_received_topic)

#!/usr/bin/env python3

import asyncio
import json

from pymongo.errors import DuplicateKeyError
import requests

from config import CONTRACTS, connector, db
from addrs import PB_ADDRS


addr = PB_ADDRS[0]
contract = CONTRACTS["pixelboard"]


async def buy_events():
    print("buy events")
    px_updated_topic = contract.get_events()[0].get_signature()
    await get_event("px", px_updated_topic.hex())
    await asyncio.sleep(1)


async def value_received_events():
    print("value events")
    value_received_topic = contract.get_events()[1].get_signature()
    await get_event("value", value_received_topic.hex())
    await asyncio.sleep(1)


async def get_event(topic_name, topic):
    print(topic_name)
    events_seen = {PB_ADDR: {"pixels": set(), "values": set()} for PB_ADDR in PB_ADDRS}

    while True:
        for PB_ADDR in PB_ADDRS:
            count = 0

            while True:
                print("looking for events on: ", PB_ADDR)
                await asyncio.sleep(1)
                best_block = connector.get_block("best")["number"]

                data = {
                    "range": {"unit": "block", "from": 0, "to": best_block},
                    "criteriaSet": [
                        {
                            "address": PB_ADDR,
                            "topic0": topic,
                        }
                    ],
                    "order": "asc",
                }

                resp = requests.post("http://localhost:8669/logs/event", json=data)
                j = json.loads(resp.content)

                if j == []:
                    asyncio.sleep(1)
                    break

                decoded_events = []

                for evt in j:
                    decoded_events.append(await decode_event(evt))

                events_seent = await pass_event_to_mongo(
                    topic_name, decoded_events, events_seen, PB_ADDR
                )


async def pass_event_to_mongo(topic_name, decoded_events, events_seen, PB_ADDR):
    for decoded in decoded_events:
        if topic_name == "px":
            doc = {
                "address": decoded["address"],
                "owner": decoded["owner"],
                "color": "#" + decoded["color"].rstrip(b"\x00").decode(),
            }

            if doc["id"] in events_seen[PB_ADDR]["pixels"]:
                continue

            try:
                coll = db[PB_ADDR]
                coll.update(
                    {},
                    {"$addToSet": {"pixels": doc}},
                    upsert=True,
                )
                print("id: ", decoded["index"])
                print("owner: ", decoded["owner"])
                print("color: ", decoded["color"].rstrip(b"\x00").decode())
                print("")

                asyncio.sleep(0)

                events_seen[PB_ADDR]["pixels"].add(decoded["index"])
            except DuplicateKeyError:
                print("ignoring duplicate key")
        elif topic_name == "value":
            doc = {
                "user": decoded["user"],
                "amount": decoded["amount"] // (10 ** 18),
            }

            try:
                coll = db[PB_ADDR]
                coll.update(
                    {},
                    {"$push": {"values": doc}},
                )
                print("user: ", decoded["user"])
                print("amount: ", decoded["amount"] // (10 ** 18))
                print("")

                asyncio.sleep(0)

            except DuplicateKeyError:
                print("ignoring duplicate key")

    return events_seen


async def decode_event(event):
    event_obj = contract.get_event_by_signature(bytes.fromhex(event["topics"][0][2:]))
    decoded = event_obj.decode(
        bytes.fromhex(event["data"][2:]),
        [bytes.fromhex(topic[2:]) for topic in event["topics"]],
    )

    return decoded


async def main():
    # await buy_events()
    await value_received_events()


if __name__ == "__main__":
    asyncio.run(main())

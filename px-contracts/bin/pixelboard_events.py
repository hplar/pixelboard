#!/usr/bin/env python3

import json
from time import sleep

from pymongo.errors import DuplicateKeyError

import requests

from addrs import FACT_ADDR
from config import CONTRACTS, connector, db

contract = CONTRACTS["factory"]


def decode_event(event):
    event_obj = contract.get_event_by_signature(bytes.fromhex(event["topics"][0][2:]))
    decoded = event_obj.decode(
        bytes.fromhex(event["data"][2:]),
        [bytes.fromhex(topic[2:]) for topic in event["topics"]],
    )

    return decoded


created_topic = contract.get_events()[1].get_signature()
count = 0

while True:
    print("Looking for events")

    best_block = connector.get_block("best")["number"]

    data = {
        "range": {"unit": "block", "from": 0, "to": best_block},
        "options": {"offset": count, "limit": 1},
        "criteriaSet": [
            {
                "address": FACT_ADDR,
                "topic0": created_topic.hex(),
            }
        ],
        "order": "asc",
    }

    resp = requests.post("http://localhost:8669/logs/event", json=data)
    j = json.loads(resp.content)

    if j == []:
        sleep(0.2)
        continue

    for a in j:
        decoded = decode_event(a)

        db_doc = {
            "_id": decoded["id"],
            "address": decoded["pixelboardAddress"],
            "title": decoded["title"].rstrip(b"\x00").decode(),
            "width": decoded["width"],
            "height": decoded["height"],
            "size": decoded["width"] * decoded["height"],
            "filled": False,
            "pixels": [],
        }

        try:
            db[decoded["pixelboardAddress"]].insert_one(db_doc)
            print("id: ", db_doc["_id"])
            print("address: ", db_doc["address"])
            print("title: ", db_doc["title"])
            print("width: ", db_doc["width"])
            print("height :", db_doc["height"])
            print("")
        except DuplicateKeyError:
            print("ignoring duplicate key")

        count += 1

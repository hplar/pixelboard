#!/usr/bin/env python3

width = 11
height = 11


grid = [
    [
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
    ]
    for _ in range(height)
]

cheap_row_top = range(0, len(grid) // 5)
cheap_row_bottom = range(len(grid) - len(grid) // 5, len(grid))
cheap_row_left = range(0, len(grid) // 5)
cheap_row_right = range(len(grid) - len(grid) // 5, len(grid))

medium_row_top = range(len(grid) // 5, (len(grid) // 5) * 2)
medium_row_bottom = range(len(grid) - (len(grid) // 5) * 2, len(grid))
medium_row_left = range(len(grid) // 5, (len(grid) // 5) * 2)
medium_row_right = range(len(grid) - (len(grid) // 5) * 2, len(grid))

for i, row in enumerate(grid):
    if i in cheap_row_top:
        for j, cell in enumerate(row):
            row[j] = 1
    if i in cheap_row_bottom:
        for j, cell in enumerate(row):
            row[j] = 1

    else:
        for j, cell in enumerate(row):
            if j in cheap_row_left:
                row[j] = 1
            elif j in cheap_row_right:
                row[j] = 1

            if i in medium_row_top:
                if j in medium_row_left:
                    row[j] = 2
                elif j in medium_row_right:
                    row[j] = 2
            if i in medium_row_bottom:
                if j in medium_row_left:
                    row[j] = 2
                elif j in medium_row_right:
                    row[j] = 2


for r in grid:
    print(r)

# for i, row in enumerate(grid):
#     if i < height / 3 or i > 3 + height / 3:
#         continue
#     for j, cell in enumerate(row):
#         if j > width / 3 and j < 3 + width / 3:
#             row[j] = 2

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Burnable.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

/**
 * @dev Factory contract that tracks, deploys and manages Pixelboards.
 */
contract PixelboardFactory is Ownable {
    event PixelboardCreated(
        uint256 indexed id,
        address indexed pixelboardAddress,
        bytes32 title,
        uint8 width,
        uint8 height
    );

    uint16 public boardCount = 0;
    mapping(uint256 => address) pixelboards;

    function createPixelboard(
        uint256 _id,
        uint8 _width,
        uint8 _height,
        bytes32 _title
    ) external onlyOwner returns (address newPixelBoard) {
        Pixelboard pb = new Pixelboard(_width, _height, _title);
        pixelboards[_id] = address(pb);

        emit PixelboardCreated(_id, address(pb), _title, _width, _height);

        boardCount++;

        return address(pb);
    }

    function getPixelboardAddress(uint256 _id) external view returns (address) {
        return pixelboards[_id];
    }

    function getAddresses() external view returns (address[] memory) {
        address[] memory ret = new address[](boardCount);
        for (uint256 i = 0; i < boardCount; i++) {
            ret[i] = pixelboards[i];
        }
        return ret;
    }
}

/**
 * @dev Pixelboard contract to keep state of a pixelboard.
 */
contract Pixelboard is Ownable {
    event ValueReceived(address indexed user, uint256 amount);
    event PixelSold(
        uint256 indexed index,
        address owner,
        bytes6 color,
        uint256 price
    );

    bytes32 public title;
    uint32 public width;
    uint32 public height;
    uint256 public startPrice = 5_000_000_000_000_000_000;
    uint32 public qtySold = 0;
    bool public completed = false;
    uint256 public qtyVet = 0;
    uint256 public qtyPixels;
    address payable wallet;

    mapping(uint256 => bytes6) public colors;
    mapping(address => uint8) public qtyPixelsOfAddr;
    mapping(address => uint256) public qtyVetByAddr;
    mapping(address => uint256) public addrCurrentPrice;

    receive() external payable {
        emit ValueReceived(msg.sender, msg.value);
    }

    constructor(
        uint8 _width,
        uint8 _height,
        bytes32 _title
    ) {
        width = _width;
        height = _height;
        qtyPixels = width * height;
        title = _title;
        wallet = payable(msg.sender);
    }

    function buyPixel(uint256 _id, bytes6 _color) external payable {
        require(!completed, "Pixelboard is complete");
        require(colors[_id] == 0x0, "Pixel already has an owner");
        require(
            qtyPixelsOfAddr[msg.sender] < 10,
            "Address already at max pixels"
        );
        if (addrCurrentPrice[msg.sender] != 0) {
            require(
                msg.value >= addrCurrentPrice[msg.sender],
                "Not enough VET in transaction"
            );
        } else {
            require(msg.value == startPrice, "Not enough VET in transaction");
            addrCurrentPrice[msg.sender] = startPrice;
        }

        colors[_id] = _color;

        if (qtySold == qtyPixels) {
            completed = true;
        }

        qtySold++;
        qtyVet += msg.value;
        qtyVetByAddr[msg.sender] += msg.value;
        qtyPixelsOfAddr[msg.sender] += 1;

        _decreaseAddrCurrentPrice(msg.sender);

        emit PixelSold(_id, msg.sender, _color, msg.value);

        if (qtySold == qtyPixels) {
            completed = true;
        }
    }

    function _decreaseAddrCurrentPrice(address _addr) private {
        if (addrCurrentPrice[_addr] != 1e18) addrCurrentPrice[_addr] -= 1e18;
    }

    function destroy() public onlyOwner {
        title = bytes32("Destroyed");
        width = 0;
        height = 0;
        qtySold = 0;
        completed = true;
        qtyVet = 0;
        qtyPixels = 0;
    }

    function withdraw() public onlyOwner {
        wallet.transfer(qtyVet);
    }

    function getAddrCurrentPrice(address _addr)
        public
        view
        returns (uint256 currentPrice)
    {
        if (addrCurrentPrice[_addr] == 0) {
            return startPrice;
        } else {
            return addrCurrentPrice[_addr];
        }
    }
}

/**
 * @dev ERC721 contract to create and manage Pixelboard NFTs.
 */
contract TokenFactory is ERC721URIStorage, Ownable {
    using Counters for Counters.Counter;
    Counters.Counter private _tokenIds;

    constructor(string memory name, string memory symbol)
        ERC721(name, symbol)
    {}

    uint256 public qtyNfts = 0;

    function createToken(address recipient, string memory metadata)
        public
        onlyOwner
        returns (uint256)
    {
        _tokenIds.increment();
        uint256 newItemId = _tokenIds.current();
        _safeMint(recipient, newItemId);
        _setTokenURI(newItemId, metadata);

        qtyNfts++;

        return newItemId;
    }
}

import pytest


@pytest.fixture(autouse=True)
def isolation(fn_isolation):
    """This fixtures makes sure every test function uses a clean snapshot
    of the environment, preventing tests from influencing eachother."""
    ...


@pytest.fixture
def factory(PixelboardFactory, accounts):
    return accounts[0].deploy(PixelboardFactory)


@pytest.fixture
def pixelboard_extra_small(Pixelboard, accounts):
    return accounts[0].deploy(Pixelboard, 2, 2, "Test board".encode())


@pytest.fixture
def pixelboard_small(Pixelboard, accounts):
    return accounts[0].deploy(Pixelboard, 8, 8, "Test board".encode())


@pytest.fixture
def pixelboard_medium(Pixelboard, accounts):
    return accounts[0].deploy(Pixelboard, 16, 16, "Test board".encode())


@pytest.fixture
def pixelboard_large(Pixelboard, accounts):
    return accounts[0].deploy(Pixelboard, 32, 32, "Test board".encode())


@pytest.fixture
def token_factory(TokenFactory, accounts):
    return accounts[0].deploy(TokenFactory, "PixelboardToken", "PBN")

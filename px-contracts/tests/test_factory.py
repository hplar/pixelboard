import brownie
from brownie.convert.datatypes import HexString


class TestPixelboardFactory:
    def test_deployment(self, factory, accounts):
        assert factory.boardCount() == 0

    def test_createPixelboard(self, factory, accounts):
        factory.createPixelboard(0, 8, 8, "Test board".encode())
        assert factory.boardCount() == 1

    def test_getPixelboardAddress(self, factory, accounts):
        tx = factory.createPixelboard(0, 8, 8, "Test board".encode())
        brownie.Pixelboard.at(tx.return_value)

    def test_getAddresses(self, factory, accounts):
        for i in range(3):
            tx = factory.createPixelboard(i, 8, 8, "Test board".encode())
            brownie.Pixelboard.at(tx.return_value)

        assert len(factory.getAddresses()) == 3

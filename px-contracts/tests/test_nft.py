import brownie


class TestNFT:
    def test_deployment(self, token_factory):
        assert token_factory.qtyNfts() == 0

    def test_createToken(self, token_factory):
        tx = token_factory.createToken(brownie.accounts[1], "https://fakeurl.pb")
        assert token_factory.qtyNfts() == 1
        assert token_factory.ownerOf(tx.return_value) == brownie.accounts[1]
        assert token_factory.tokenURI(tx.return_value) == "https://fakeurl.pb"

    def test_createToken_onlyOwner(self, token_factory):
        with brownie.reverts("Ownable: caller is not the owner"):
            token_factory.createToken(
                brownie.accounts[0], "https://fakeurl.pb", {"from": brownie.accounts[3]}
            )

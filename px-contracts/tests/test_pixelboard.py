import brownie
from brownie.convert.datatypes import HexString


class TestSmallPixelboard:

    START_PRICE = 5e18
    DIVIDER = 100
    MULTIPLIER = 3

    def test_deployment(self, pixelboard_small, accounts):
        pb = pixelboard_small

        title = pb.title()
        width = pb.width()
        height = pb.height()
        completed = pb.completed()
        qtyPixels = pb.qtyPixels()
        qtySold = pb.qtySold()

        assert title == HexString(b"Test board", "bytes32")
        assert width == 8
        assert height == 8
        assert completed == False
        assert qtyPixels == 64
        assert qtySold == 0

    def test_destruction(self, pixelboard_small, accounts):
        pb = pixelboard_small
        assert pb.title() == HexString(b"Test board", "bytes32")
        assert pb.width() == 8
        assert pb.height() == 8
        assert pb.completed() == False
        assert pb.qtyPixels() == 64

        pb.buyPixel(0, "ff0000".encode(), {"value": self.START_PRICE})

        assert pb.qtySold() == 1
        assert pb.qtyVet() == 5e18

        pb.destroy()

        # TODO: check if title value is the right type.
        assert pb.title().rstrip(b"\x00") == b"Destroyed"
        assert pb.width() == 0
        assert pb.height() == 0
        assert pb.completed() == True
        assert pb.qtyPixels() == 0
        assert pb.qtySold() == 0

    def test_destruction_onlyOwner(self, pixelboard_small, accounts):
        pb = pixelboard_small

        with brownie.reverts("Ownable: caller is not the owner"):
            pb.destroy({"from": accounts[1]})

    def test_buyPixel(self, pixelboard_small, accounts):
        pb = pixelboard_small

        pb.buyPixel(0, "ff0000".encode(), {"value": self.START_PRICE})
        assert pb.colors(0) == HexString(b"ff0000", "bytes6")

    def test_buyPixel_decreasing_price(self, pixelboard_small, accounts):
        pb = pixelboard_small

        for i in range(4):
            pb.buyPixel(
                i,
                "ff0000".encode(),
                {"value": self.START_PRICE - i * 1e18, "from": accounts[0]},
            )
        assert pb.addrCurrentPrice(accounts[0]) == 1e18

    def test_buyPixel_decreasing_price_minimum_price_1_vet(
        self, pixelboard_small, accounts
    ):
        pb = pixelboard_small

        for i in range(4):
            pb.buyPixel(
                i,
                "ff0000".encode(),
                {"value": self.START_PRICE - i * 1e18, "from": accounts[0]},
            )

        for i in range(4, 7):
            pb.buyPixel(
                i,
                "ff0000".encode(),
                {"value": 1e18, "from": accounts[0]},
            )

        assert pb.qtySold() == 7

    def test_buyPixel_not_enough_vet(self, pixelboard_small, accounts):
        pb = pixelboard_small

        with brownie.reverts("Not enough VET in transaction"):
            pb.buyPixel(0, "ff0000".encode(), {"value": 4_999_999_999_999_999_999})

    def test_buyPixel_has_owner(self, pixelboard_small, accounts):
        pb = pixelboard_small

        pb.buyPixel(0, "ff0000".encode(), {"value": self.START_PRICE})

        with brownie.reverts("Pixel already has an owner"):
            pb.buyPixel(
                0,
                "ff0000".encode(),
                {
                    "value": self.START_PRICE
                    + (self.START_PRICE / self.DIVIDER) * self.MULTIPLIER
                },
            )

    def test_qtyVet_is_tracked(self, pixelboard_small):
        pb = pixelboard_small

        for i in range(3):
            pb.buyPixel(i, "ff0000".encode(), {"value": self.START_PRICE - i * 1e18})

        assert pb.qtyVet() == 12e18

    def test_qtySold_is_tracked(self, pixelboard_small):
        pb = pixelboard_small

        for i in range(3):
            pb.buyPixel(i, "ff0000".encode(), {"value": self.START_PRICE - i * 1e18})

        assert pb.qtySold() == 3

    def test_qtyVetPerPixel_owner_is_tracked(self, pixelboard_small, accounts):
        pb = pixelboard_small

        for i in range(3):
            pb.buyPixel(
                i,
                "ff0000".encode(),
                {"value": self.START_PRICE - i * 1e18, "from": accounts[0]},
            )

        pb.buyPixel(
            3,
            "ff0000".encode(),
            {"value": self.START_PRICE, "from": accounts[1]},
        )

        first_account_expected = 12e18
        second_account_expected = 5e18

        assert pb.qtyVetByAddr(accounts[0]) == first_account_expected
        assert pb.qtyVetByAddr(accounts[1]) == second_account_expected
        assert pb.qtyVet() == first_account_expected + second_account_expected

    def test_withdraw(self, pixelboard_small, accounts):
        pb = pixelboard_small

        current_balance = accounts[0].balance()

        for i in range(2):
            pb.buyPixel(
                i,
                "ff0000".encode(),
                {"value": self.START_PRICE - i * 1e18, "from": accounts[1]},
            )

        qtyVet = pb.qtyVet()
        pb.withdraw()

        assert accounts[0].balance() == current_balance + qtyVet

    def test_maximum_amount_of_pixels_sold_to_address(self, pixelboard_small, accounts):
        pb = pixelboard_small

        for i in range(4):
            pb.buyPixel(
                i,
                "ff0000".encode(),
                {"value": self.START_PRICE - i * 1e18, "from": accounts[0]},
            )

        for i in range(4, 10):
            pb.buyPixel(
                i,
                "ff0000".encode(),
                {"value": 1e18, "from": accounts[0]},
            )

        with brownie.reverts("Address already at max pixels"):
            pb.buyPixel(11, "ff0000".encode(), {"value": 1e18, "from": accounts[0]})

    def test_getAddrCurrentPrice(self, pixelboard_small, accounts):
        pb = pixelboard_small

        addrCurrentPrice = pb.getAddrCurrentPrice(accounts[0])
        assert addrCurrentPrice == 5e18

        pb.buyPixel(
            0,
            "ff0000".encode(),
            {"value": addrCurrentPrice, "from": accounts[0]},
        )

        newAddrCurrentPrice = pb.getAddrCurrentPrice(accounts[0])
        assert newAddrCurrentPrice == 4e18


class TestExtraSmallBoard:
    START_PRICE = 5e18

    def test_pixelboard_completed(self, pixelboard_extra_small, accounts):
        pb = pixelboard_extra_small

        for i in range(4):
            pb.buyPixel(
                i,
                "ff0000".encode(),
                {"value": self.START_PRICE - i * 1e18, "from": accounts[0]},
            )

        assert pb.completed() == True

        with brownie.reverts("Pixelboard is complete"):
            pb.buyPixel(4, "ff0000".encode(), {"value": 1e18})

    def test_highest_qtyVet(self, pixelboard_extra_small, accounts):
        pb = pixelboard_extra_small

        accounts[1].transfer(accounts[0].address, accounts[1].balance())

        for i in range(pb.qtyPixels()):
            pb.buyPixel(
                i,
                "ff0000".encode(),
                {"value": self.START_PRICE - i * 1e18, "from": accounts[0]},
            )

        assert pb.qtyVet() == 14e18  # TODO: not sure if this amount is correct

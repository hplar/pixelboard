from messenger.blockchain.events.schemas import PixelboardCreated
from messenger.blockchain.events.utils import fetch_events
from messenger.kafka.utils import publish_factory_event
from messenger.config import contracts, connector


async def produce_factory_events(app):
    """
    Reads the events from the blockchain publishes them on a kafka topic.
    After producing it should store the id in a local DB (rocksDB?)
    so we can attempt to prevent duplicate events on the topic.
    """

    kafka_topic = app.topic("factory", value_type=PixelboardCreated)

    pixelboard_created_topic = contracts.factory.contract.get_events()[1].get_signature()
    best_block = connector.get_block("best")["number"]

    data = {
        "range": {"unit": "block", "from": 0, "to": best_block},
        "criteriaSet": [
            {
                "address": contracts.factory.address,
                "topic0": pixelboard_created_topic.hex(),
            }
        ],
        "order": "asc",
    }

    for event in await fetch_events(contracts.factory.contract, data):
        await publish_factory_event(kafka_topic, event)

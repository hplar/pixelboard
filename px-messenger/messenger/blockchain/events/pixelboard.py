from messenger.blockchain.events.schemas import PixelSold
from messenger.blockchain.events.utils import (
    fetch_events,
    get_best_block,
)
from messenger.kafka.utils import publish_pixel_updated_event
from messenger.config import contracts, redis_client


async def produce_pixel_events(app):
    """
    Reads the events from the blockchain publishes them on a kafka topic.
    After producing it should store the id in a local DB (rocksDB?)
    so we can attempt to prevent duplicate events on the topic.
    """
    pixelboard_addresses = [
        address.decode() for address in redis_client.lrange("pixelboards", 0, -1)
    ]

    for pixelboard_address in pixelboard_addresses:
        print(f"[PIXELEVENTS] checking {pixelboard_address}")
        kafka_topic = app.topic(pixelboard_address, value_type=PixelSold)

        pixels_sold_topic = contracts.pixelboard.contract.get_events()[1].get_signature()

        data = {
            "range": {"unit": "block", "from": 0, "to": await get_best_block()},
            "criteriaSet": [
                {
                    "address": pixelboard_address,
                    "topic0": pixels_sold_topic.hex(),
                }
            ],
            "order": "asc",
        }

        for event in await fetch_events(contracts.pixelboard.contract, data):
            print(f"[PIXELEVENTS] Fetched pixel event {event['index']}")
            await publish_pixel_updated_event(kafka_topic, pixelboard_address, event)

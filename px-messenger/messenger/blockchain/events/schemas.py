import faust


class PixelboardCreated(faust.Record):
    def __abstract_init__(self) -> None:
        pass

    id: int
    title: str
    address: str
    width: int
    height: int


class PixelSold(faust.Record):
    def __abstract_init__(self) -> None:
        pass

    index: int
    owner: str
    color: str
    price: int

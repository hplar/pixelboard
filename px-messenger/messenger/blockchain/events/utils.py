import json

import requests

from messenger.config import connector


def decode_event(contract, event):
    """Decodes events from the blockchain"""

    event_obj = contract.get_event_by_signature(bytes.fromhex(event["topics"][0][2:]))
    decoded = event_obj.decode(
        bytes.fromhex(event["data"][2:]),
        [bytes.fromhex(topic[2:]) for topic in event["topics"]],
    )

    return decoded


async def fetch_events(contract, data):
    """
    Fetches events from the blockchain by calling its logs/events endpoint
    """

    resp = requests.post("http://localhost:8669/logs/event", json=data)
    return [decode_event(contract, event) for event in json.loads(resp.content)]


async def get_best_block():
    """
    Get the current best block so we can use it to fetch events
    """

    return connector.get_block("best")["number"]

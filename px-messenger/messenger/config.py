import redis
from dotenv import dotenv_values
from thor_requests.connect import Connect
from thor_requests.contract import Contract
from thor_requests.wallet import Wallet

env = dotenv_values(".env.blockchain_addresses")


class Factory:
    def __init__(self):
        self.contract: Contract = Contract.fromFile("messenger/blockchain/abis/PixelboardFactory.json")
        self.address: str = env.get("FACT_ADDR")


class Pixelboard:
    def __init__(self):
        self.contract = Contract.fromFile("messenger/blockchain/abis/Pixelboard.json")


class Contracts:
    def __init__(self, factory, pixelboard):
        self.factory = factory
        self.pixelboard = pixelboard


mnemonic = [
    "denial",
    "kitchen",
    "pet",
    "squirrel",
    "other",
    "broom",
    "bar",
    "gas",
    "better",
    "priority",
    "spoil",
    "cross",
]

contracts = Contracts(factory=Factory(), pixelboard=Pixelboard())
THOR_URL = "http://localhost:8669"
wallet = Wallet.fromMnemonic(mnemonic)
connector = Connect(THOR_URL)
redis_client = redis.Redis(host="localhost", port=6379, db=0)

from messenger.blockchain.events.schemas import PixelboardCreated, PixelSold
from messenger.config import redis_client


async def publish_factory_event(topic, event):
    """
    Publish factory related events to Kafka
    """

    id = event["id"]
    title = event["title"].decode()
    address = event["pixelboardAddress"]
    width = event["width"]
    height = event["height"]

    if address in [addr.decode() for addr in redis_client.lrange("pixelboards", 0, -1)]:
        return

    try:
        await topic.send(
            value=PixelboardCreated(
                id=id,
                title=title,
                address=address,
                width=width,
                height=height,
            )
        )
    except:
        return

    print(f"[FACTORY] Published factory event {title} {address}")
    redis_client.lpush("pixelboards", event["pixelboardAddress"])


async def publish_pixel_updated_event(topic, address, event):
    """
    Publish pixel related events to Kafka
    """
    index = event["index"]
    owner = event["owner"]
    color = event["color"]
    price = event["price"]

    for processed_pixel_index in redis_client.lrange(address, 0, -1):
        if processed_pixel_index.decode() == str(index):
            return

    try:
        await topic.send(
            value=PixelSold(index=index, owner=owner, color=color.decode(), price=price)
        )
    except:
        return

    redis_client.lpush(address, index)
    print(f"[PIXELEVENT] Published pixel event {index} {owner} {color}")

import faust

from messenger.blockchain.events.factory import produce_factory_events
from messenger.blockchain.events.pixelboard import produce_pixel_events


app = faust.App("px-faust", broker=["kafka://localhost"], web_port=6067)
topic = app.topic("factory")


@app.timer(2)
async def factory_info(app):
    print("[PRODUCER] Factory events")
    await produce_factory_events(app)


@app.timer(3)
async def pixel_update(app):
    print("[PRODUCER] Pixel events")
    await produce_pixel_events(app)


app.main()

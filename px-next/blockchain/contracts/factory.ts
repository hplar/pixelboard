import connex, { factoryAddress } from "blockchain/config";
import { Contract, ContractMethodParams } from "types/vechain";

const definition = require("blockchain/contracts/abis/PixelboardFactory.json")

const FactoryContract: Contract = {}

definition.abi.forEach((method: any) => {
  if (method.type !== 'function') {
    return
  }

  if (method.stateMutability === 'view') {
    FactoryContract[method.name] = defineConstant(method)
  } else {
    FactoryContract[method.name] = defineSignedRequest(method)
  }
})

function defineConstant(method: any): (params: ContractMethodParams) => Promise<Connex.VM.Output & Connex.Thor.Account.WithDecoded> {
  return (params: ContractMethodParams) => connex.thor.account(params.address as string).method(method).call()
}

function defineSignedRequest (method: any) {
  return async (...args: any) => {
    const transferClause = connex.thor.account(factoryAddress).method(method).asClause(...args)
    const signingService = connex.vendor.sign('tx', [transferClause])

    return await signingService
        .gas(3_000_000)
        .request()
  }
}

export default FactoryContract
import connex from "blockchain/config"
import { Contract, ContractMethodParams } from "types/vechain";

const definition = require("blockchain/contracts/abis/Pixelboard.json")

const PixelboardContract: Contract = {}

definition.abi.forEach((method: any) => {
    if (method.type !== 'function') {
        return
    }

    if (method.stateMutability === 'view') {
        PixelboardContract[method.name] = defineConstant(method)
    } else {
        PixelboardContract[method.name] = defineSignedRequest(method)
    }
})

function defineConstant(method: any): (params: ContractMethodParams) => Promise<Connex.VM.Output & Connex.Thor.Account.WithDecoded> {
    if (method.name === "getAddrCurrentPrice") {
        return (params: ContractMethodParams) => connex.thor.account(params.address as string).method(method).call(params.sender)
    } else if (method.name === "qtyVetByAddr") {
        return (params: ContractMethodParams) => connex.thor.account(params.address as string).method(method).call(params.sender)
    } else {
        return (params: ContractMethodParams) => connex.thor.account(params.address as string).method(method).call()
    }
}

function defineSignedRequest (method: any) {
    return async (args: ContractMethodParams): Promise<Connex.Vendor.TxResponse> => {
        const { id, color, amount, txComment, address } = args
        const transferClause = connex.thor.account(address as string).method(method).asClause(id, color)
        transferClause.value = amount!.toString()
        const signingService = connex.vendor.sign('tx', [transferClause])

        return await signingService
            .comment(txComment as string)
            .gas(150_000)
            .request()
    }
}

export default PixelboardContract
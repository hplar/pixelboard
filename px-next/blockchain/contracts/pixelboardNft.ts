import connex from "blockchain/config"
import { Contract, ContractMethodParams } from "types/vechain";

const definition = require("blockchain/contracts/PixelboardToken.json")

const NftContract: Contract = {}

definition.abi.forEach((method: any) => {
    if (method.type !== 'function') {
        return
    }

    if (method.stateMutability === 'view') {
        NftContract[method.name] = defineConstant(method)
    } else {
        NftContract[method.name] = defineSignedRequest(method)
    }
})

function defineConstant(method: any): (params: ContractMethodParams) => Promise<Connex.VM.Output & Connex.Thor.Account.WithDecoded> {
    return (params: ContractMethodParams) => connex.thor.account(params.address as string).method(method).call(1)
}

function defineSignedRequest (method: any) {
    return async (args: ContractMethodParams): Promise<Connex.Vendor.TxResponse> => {
        const { nftAddress, tokenRecipient, metadata, txComment } = args
        const transferClause = connex.thor.account(nftAddress as string).method(method).asClause(tokenRecipient, metadata)
        const signingService = connex.vendor.sign('tx', [transferClause])

        return await signingService
            .comment(txComment as string)
            .gas(250_000)
            .request()
    }
}

export default NftContract
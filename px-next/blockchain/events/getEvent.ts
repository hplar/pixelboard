import connex from "blockchain/config";

const pbDefinition = require("blockchain/contracts/abis/Pixelboard.json")


export function getPixelboardCreatedEvent(i: number, timeout: number, filter: Connex.Thor.Filter<"event", Connex.Thor.Account.WithDecoded>) {

    return new Promise<void>((resolve) => setTimeout(function(){
        filter.apply(i, 1)
            .then((resp: any) => {
                for (const event of resp) {
                    console.log("Fetched a pixelboard event")
                }
            }
        )
        resolve();
    }, timeout))
}


export function getPixelUpdatedEvent(boardId: number, address: string, i: number) {
    const pixelUpdateABI = pbDefinition.abi[1]
    const acc = connex.thor.account(address)
    const event = acc.event(pixelUpdateABI)
    const filter = event.filter([{}])

    return new Promise<void>((resolve) => setTimeout(function(){
        filter.apply(i, 1)
            .then((resp: any) => {
                for (const event of resp) {
                    console.log("fetched a pixel event")
                }
            })
        resolve();
    }, 100))
}
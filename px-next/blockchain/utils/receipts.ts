import connex from "blockchain/config"

const waitForNextBlock = () => connex.thor.ticker().next()
const getTransactionReceipt = (id: string) => connex.thor.transaction(id).getReceipt()

export const waitForTransactionReceipt = async (id: string): Promise<any> => {
    do {
        await waitForNextBlock()
        const transaction = await getTransactionReceipt(id)
        if (transaction) {
            return transaction
        }
    } while (true)
}
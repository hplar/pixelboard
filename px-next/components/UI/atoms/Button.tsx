import { ReactElement, ReactNode } from "react";
import { createUseStyles } from "react-jss";

type NameRules = 'base' | 'enabled' | 'disabled'

type ButtonStyleProps = {
  active?: boolean
}

type ButtonProps = ButtonStyleProps & {
  onClick?: () => void,
  children?: ReactNode
  disabled?: boolean
}

const useStyles = createUseStyles<NameRules, ButtonStyleProps>({
  base: (props: ButtonStyleProps) => ({
    background: props.active ? "#41497890" : "#414978",
    border: "solid 2px #81bccc00",
    color: "#fff",
    padding: 16,
    fontWeight: "bold",
    transition: ['transform', 'background', 'border', 'color'],
    transitionDuration: 400,
  }),
  enabled: {
    '&:hover': {
      background: "#41497880",
      border: "solid 2px #4147880",
      transition: ['transform', 'background', 'border', 'color'],
      transitionDuration: 400,
    }
  },
  disabled: {
    '&:hover': {
      background: "#c96d6d",
      border: "solid 2px #c96d6d40",
      transition: ['transform', 'background', 'border', 'color'],
      transitionDuration: 400,
    }
  }
})


const Button = (props: ButtonProps): ReactElement => {
  const classes = useStyles({
    active: props.active
  })

  const className = [
    classes.base,
    props.disabled ? classes.disabled : classes.enabled
  ].filter(Boolean).join(' ')

  return (
    <button onClick={props.onClick} className={className} disabled={props.disabled}>
      { props.children }
    </button>
    )
}

export default Button
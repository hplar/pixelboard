import { Dispatch, ReactElement, SetStateAction, useEffect, useState } from "react";
import { HexColorPicker } from "react-colorful";
import { useAppDispatch, useAppSelector } from "store/hooks";
import { setPickerColor } from "store/slices/PixelControls";
import { PixelsSSRData } from "types/ssr";


export default function ColorPicker(): ReactElement {
  const [ color, setColor ] = useState("#aabbcc");
  const dispatch = useAppDispatch()

  const useSetPickerColor = (sColor: string) => {
    setColor(sColor)
    dispatch(setPickerColor(color))
  }

  return (
    <div>
      <HexColorPicker color={color} onChange={useSetPickerColor} />
      <p>{ color }</p>
    </div>
  )
}
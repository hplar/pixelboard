import FlexBox from "components/layout/FlexBox";
import { memo, ReactElement, useMemo } from "react";
import { createUseStyles } from "react-jss";
import { useAppDispatch, useAppSelector } from "store/hooks";
import { setActivePixel } from "store/slices/PixelControls";


type RuleNames = "pixel"

type PixelStyleProps = {
  color: string
  gridArea: string
  raster: boolean
  active: boolean
}

const useStyles = createUseStyles<RuleNames, PixelStyleProps>({
  pixel: (props: PixelStyleProps) => ({
    border: props.raster ? '1px dotted #33333390' : 'none',
    background: props.color,
    gridArea: props.gridArea,
    fontSize: 12,
    opacity: 1,
    textAlign: 'center',
    textOverflow: 'hidden',
    transition: ['transform', 'opacity'],
    transitionTimingFunction: 'ease-in-out',
    transitionDuration: 300,
    "&:hover": {
      opacity: props.active ? 1 : 0.30,
      transitionDuration: 300
    }}),
}, { link: true })


interface PixelProps {
  id: number
}

const Pixel = (props: PixelProps): ReactElement => {
  const { id } = props

  const { color, isOwned, price } = useAppSelector((state) => state.pixels.pixels[id])
  const { identifiers, prices, raster } = useAppSelector((state) => state.gridControls)
  const { activeId, pickerColor } = useAppSelector((state) => state.pixelControls)

  const dispatch = useAppDispatch()

  let gridArea = `p${id}`
  let colorOverride = "#" + color

  if (activeId === id && !isOwned && pickerColor !== undefined) {
    colorOverride = pickerColor
  } else if (color === 'None') {
    colorOverride = '#ffffff00'
  }

  const classes = useStyles(
    {
      color: colorOverride,
      gridArea,
      raster,
      active: activeId === id
  })

  // Memoize this to prevent unnecessary re-renders
  return useMemo(() => {
    console.log("pixel rendered")

    const useSetActivePixelId = async () => {
      await dispatch(setActivePixel({ id, isOwned }))
    }

    return (
      <div className={classes.pixel} onClick={useSetActivePixelId}>
        <FlexBox alignItems={'center'} justifyContent={'center'} fill>
          <div>{identifiers && id}</div>
          <div>{prices && price}</div>
        </FlexBox>
      </div>
    )
  }, [classes.pixel, dispatch, id, identifiers, isOwned, price, prices])
}

export default Pixel
import React, { ComponentProps, ReactNode } from 'react';
import { Story, Meta } from "@storybook/react";

import Button from "../Button";

export default {
  title: 'Atoms/Button',
  decorators: [],
  component: Button
} as Meta

const Template: Story<ComponentProps<typeof Button>> = (args) => <Button {...args} />

export const Enabled = Template.bind({});
export const Disabled = Template.bind({});

Enabled.args = {
  disabled: false,
  children: "Sample Button",
  onClick: () => { alert("Starting transaction") }
}

Disabled.args = {
  disabled: true,
  children: "Sample Button",
  onClick: () => { alert("This should not show") }
}
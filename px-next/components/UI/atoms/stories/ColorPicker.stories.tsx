import { Meta, Story } from "@storybook/react";
import ColorPicker from "components/UI/atoms/ColorPicker";
import React, { ComponentProps } from 'react';

export default {
    title: 'Atoms/ColorPicker',
    decorators: [],
    component: ColorPicker
} as Meta

const Template: Story<ComponentProps<typeof ColorPicker>> = (args) => <ColorPicker />
export const Normal = Template.bind({});

Normal.args = {
    color: "#ffaaee",
}
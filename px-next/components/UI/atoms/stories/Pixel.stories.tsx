import React, {ComponentProps} from 'react';
import { Story, Meta } from "@storybook/react";

import Pixel from '../Pixel';

export default {
    title: 'Atoms/Pixel',
    decorators: [],
    component: Pixel
} as Meta

const Template: Story<ComponentProps<typeof Pixel>> = (args) => <Pixel {...args} />

export const Normal = Template.bind({});

Normal.args = {
    id: 0,
    color: "#ffaaee",
    gridArea: "p0",
}
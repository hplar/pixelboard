import FlexBox from "components/layout/FlexBox";
import { ReactElement } from "react";
import { useAppSelector } from "store/hooks";

interface ActivePixelInfoProps {
  activePixelId: number | undefined
}

export default function ActivePixelInfo(props: ActivePixelInfoProps): ReactElement {
  const { activeId } = useAppSelector((state) => state.pixelControls)
  const { pixels } = useAppSelector((state) => state.pixels)
  const currentPrice = useAppSelector((state) => state.user.currentPrice)

  if (activeId !== undefined) {
    const { id, owner, color, price } = pixels[activeId]

    return (
      <FlexBox direction={'column'}>
        <table>
          <tbody>
          <tr>
            <td>id</td>
            <td>{id}</td>
          </tr>
          <tr>
            <td>owner</td>
            <td>{owner}</td>
          </tr>
          <tr>
            <td>color</td>
            <td style={{color: "#" + color}}>{"#" + color}</td>
          </tr>
          <tr>
            <td>Price</td>
            <td>{price ?? (currentPrice && currentPrice / 1e18) }</td>
          </tr>
          </tbody>
        </table>
      </FlexBox>
    )
  } else {
    return (
      <FlexBox direction={'column'}>
        <table>
          <tbody>
          <tr>
            <td>id</td>
            <td>No pixel selected</td>
          </tr>
          <tr>
            <td>owner</td>
            <td></td>
          </tr>
          <tr>
            <td>color</td>
            <td></td>
          </tr>
          <tr>
            <td>Price</td>
            <td></td>
          </tr>
          </tbody>
        </table>
      </FlexBox>
    )
  }
}
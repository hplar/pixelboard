import connex from "blockchain/config";
import { ReactElement } from "react";
import Button from "../atoms/Button";
import { useAppDispatch, useAppSelector } from "store/hooks";
import { setConnect, setDisconnect } from "store/slices/User";


const LoginButton = (): ReactElement => {
    const dispatch = useAppDispatch()
    const user = useAppSelector((state) => state.user)

    const login = (): void => {
        connex && connex.vendor
            .sign('cert', {
                purpose: 'identification',
                payload: {
                    type: 'text',
                    content: 'Please sign the certificate to connect your wallet.'
                }
            })
            .request()
            .then((r) => {
                dispatch(setConnect(r.annex.signer))
            })
            .catch((e) => console.log('error:' + e.message))
    }

    const logout = (): void => {
        dispatch(setDisconnect())
    }

    return (
        <>
            {
                user.connected
                    ? <Button onClick={logout}>{user.address}</Button>
                    : <Button onClick={login}>Connect wallet</Button>
            }
        </>
    )
}

export default LoginButton
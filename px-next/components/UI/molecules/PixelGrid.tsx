import Pixel from "components/UI/atoms/Pixel";
import { Dispatch, ReactElement, SetStateAction, useEffect, useState } from "react";
import { createUseStyles } from "react-jss";
import { useAppDispatch, useAppSelector } from "store/hooks";
import { setActivePixel } from "store/slices/PixelControls";
import { initGrid, setPixels, updatePixel } from "store/slices/Pixels";
import { Pixel as PX, Pixelboard } from "types/types";

type NameRules = 'pixelGrid'

const useStyles = createUseStyles<NameRules, PixelGridStyleProps>({
  pixelGrid: (props: PixelGridStyleProps) => ({
    display: 'grid',
    width: props.gridWidthPx,
    height: props.gridHeightPx,
    gridTemplateAreas: props.gridTemplateAreas,
    gridTemplateRows: props.gridTemplateRows,
    gridTemplateColumns: props.gridTemplateColumns,
    gridGap: props.gridGap,
    transition: ['transform', 'grid-gap'],
    transitionTimingFunction: 'ease-in-out',
    transitionDuration: 300,
  })
})

type PixelGridStyleProps = {
  gridTemplateAreas: string
  gridTemplateRows: string
  gridTemplateColumns: string
  gridWidthPx: string
  gridHeightPx: string
  gridGap: number
}

function initGridState(width: number, height: number): PX[] {
  const size = width * height
  let initialGridState: PX[] = new Array(size).fill(null)

  return initialGridState.map((pixel, i) => {
      return {id: i, owner: "Nobody", color: "None", price: undefined, isOwned: false}
  })
}

const initTemplate = (width: number, height: number): string => {
  let areas = ""

  let count = 0
  for (let i=0; i < height; i++) {
    let row = '\''
    for (let j=0; j < width; j++) {
      row += `p${count}    `
      count += 1
    }
    row = row.substr(0, row.length - 4)
    row += '\'\n'
    areas += row
  }

  return areas
}

interface PixelGridProps extends Pixelboard {
  activePixelId: number | undefined
  setActivePixelId: Dispatch<SetStateAction<number | undefined>>
}

export default function PixelGrid(props: PixelGridProps): ReactElement {
  const [hasEventStream, setHasEventStream] = useState<boolean>(false)
  const { _id, width, height, address } = props
  const { gap, resolution } = useAppSelector((state) => state.gridControls)
  const dispatch = useAppDispatch()
  const pixels = useAppSelector((state) => state.pixels.pixels)
  const columns = '1fr '.repeat(width)
  const rows = '1fr '.repeat(height)
  const gridWithPx = `${width * resolution}px`
  const gridHeightPx = `${height * resolution}px`
  const template = initTemplate(width, height)

  useEffect(() => {
    dispatch(setPixels(pixels))
    dispatch(initGrid(initGridState(width, height)))

    if (_id !== undefined && !hasEventStream) {
      const source = new EventSource(`/api/pixelboards/sse-pixel-events?address=${address}`)

      source.onopen = () => {
        setHasEventStream(true)
      }

      source.onmessage = (event) => {
        if (event.data !== null) {
          const { id, color, owner, price, isOwned } = JSON.parse(event.data)
          const pixel: PX = { id, color, owner, price: price / 1e18, isOwned }
          dispatch(updatePixel(pixel))
        }
      }

      return () => {
        source.close()
        setHasEventStream(false)
      }
    }
  }, [])

  const classes = useStyles({
    gridTemplateColumns: columns,
    gridTemplateRows: rows,
    gridTemplateAreas: template,
    gridGap: gap ? resolution / 4 : 0,
    gridWidthPx: gridWithPx,
    gridHeightPx: gridHeightPx,
  })


  return (
    <div className={classes.pixelGrid}>
      { pixels.map((pixel: PX) => {
        return (
          <Pixel
            key={pixel.id}
            id={pixel.id}
          />
        )
      })}
    </div>
  )
}
import Button from "components/UI/atoms/Button";

import { ReactElement, useState } from "react";
import { useAppDispatch, useAppSelector } from "store/hooks";
import { setResolution, toggleGap, toggleIdentifiers, togglePrices, toggleRaster } from "store/slices/GridControls";


export default function PixelGridControls(): ReactElement {
  const dispatch = useAppDispatch()
  const { gap, identifiers, prices, raster, resolution } = useAppSelector((state) => state.gridControls)
  const [ prevResolution, setPrevResolution ] = useState<number>()

  const toggleMaxRes = () => {
    dispatch(setResolution(36))
  }

  const toggleMinRes = () => {
    dispatch(setResolution(4))
  }

  const resUp = () => {
    dispatch(setResolution(resolution <= 32 ? resolution + 4 : resolution))
  }

  const resDown = () => {
    dispatch(setResolution(resolution >= 8 ? resolution - 4 : resolution))
  }

  const useToggleGap = () => {
    dispatch(toggleGap(!gap))
  }

  const useTogglePrices = () => {
    if (!prices) {
      dispatch(setResolution(36))
      dispatch(toggleGap(true))
      dispatch(toggleRaster(true))
      dispatch(toggleIdentifiers(false))
      setPrevResolution(resolution)
    } else {
      dispatch(toggleGap(false))
      dispatch(toggleRaster(false))
      dispatch(setResolution(prevResolution!))
    }
    dispatch(togglePrices(!prices))
  }

  const useToggleRaster = () => {
    dispatch(toggleRaster(!raster))
  }

  const useToggleIdentifiers = () => {
    if (!identifiers) {
      dispatch(setResolution(36))
      dispatch(toggleGap(true))
      dispatch(toggleRaster(true))
      dispatch(togglePrices(false))
      setPrevResolution(resolution)
    } else {
      dispatch(toggleGap(false))
      dispatch(toggleRaster(false))
      dispatch(setResolution(prevResolution!))
    }
    dispatch(toggleIdentifiers(!identifiers))
  }

  const reset = () => {
    dispatch(setResolution(16))
    dispatch(toggleGap(false))
    dispatch(toggleRaster(false))
    dispatch(toggleIdentifiers(false))
  }

  return (
    <>
      <Button onClick={resUp} disabled={resolution >= 36}>+</Button>
      <Button onClick={resDown} disabled={resolution <= 8 || identifiers}>-</Button>
      <Button onClick={toggleMaxRes}>++</Button>
      <Button onClick={toggleMinRes}>--</Button>
      <Button onClick={useToggleGap} active={gap}>GAPS</Button>
      <Button onClick={useToggleRaster} active={raster}>RASTER</Button>
      <Button onClick={useToggleIdentifiers} active={identifiers}>ID</Button>
      <Button onClick={useTogglePrices} active={prices}>PRICES</Button>
      <Button onClick={reset}>Reset</Button>
    </>
  )

}
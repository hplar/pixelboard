import PixelboardContract from "blockchain/contracts/pixelboard";
import FlexBox from "components/layout/FlexBox";
import { ReactElement, useEffect } from "react";
import { useDispatch } from "react-redux";
import { useAppSelector } from "store/hooks";
import { updatePixelboard } from "store/slices/Pixelboards";
import { setCurrentPrice } from "store/slices/User";
import { Pixelboard } from "types/types";

interface PixelboardInfoProps extends Pixelboard {}

export default function PixelboardInfo(props: PixelboardInfoProps): ReactElement {
  const dispatch = useDispatch()

  const { _id, title, address, size, width, height } = props
  const { qtySold, qtyVet, qtyVetByAddr } = useAppSelector((state) => state.pixelboards.pixelboards[_id])
  const userAddress = useAppSelector((state) => state.user.address)

  useEffect(() => {
    async function fetchPixelboardData() {
      if (address !== undefined) {
        PixelboardContract.getAddrCurrentPrice({ address: address, sender: userAddress }).then(
          (resp: any) => {
            dispatch(setCurrentPrice(resp.decoded[0]))
          }
        )

        const qtyVetByAddr = await PixelboardContract.qtyVetByAddr({ address: address, sender: userAddress })
        const qtyVet = await PixelboardContract.qtyVet({ address: address })
        const qtySold = await PixelboardContract.qtySold({ address: address })
        const completed = await PixelboardContract.completed({ address: address })

        dispatch(updatePixelboard({
          id: _id,
          qtyVetByAddr: qtyVetByAddr.decoded[0],
          qtyVet: qtyVet.decoded[0],
          qtySold: qtySold.decoded[0],
          completed: completed.decoded[0]
        }))
      }
    }
    fetchPixelboardData()
  }, [address, userAddress])

  return (
    <FlexBox direction={'column'}>
      <table>
        <tbody>
        <tr>
          <td>Title</td>
          <td>{title}</td>
        </tr>
        <tr>
          <td>Address</td>
          <td>{address}</td>
        </tr>
        <tr>
          <td>Size</td>
          <td>{size}</td>
        </tr>
        <tr>
          <td>Width</td>
          <td>{width}</td>
        </tr>
        <tr>
          <td>Height</td>
          <td>{height}</td>
        </tr>
        <tr>
          <td>qtySold</td>
          <td>{qtySold}</td>
        </tr>
        <tr>
          <td>qtyLeft</td>
          <td>{qtySold && size - qtySold}</td>
        </tr>
        <tr>
          <td>qtyVET</td>
          <td>{qtyVet && qtyVet / 1e18}</td>
        </tr>
        <tr>
          <td>qtyVetByAddr</td>
          <td>{qtyVetByAddr && qtyVetByAddr / 1e18}</td>
        </tr>
        <tr>
          <td>%VetByAddr</td>
          <td>{qtyVetByAddr && qtyVet && ((qtyVetByAddr / 1e18) / (qtyVet / 1e18)) * 100 }</td>
        </tr>
        </tbody>
      </table>
    </FlexBox>
  )
}
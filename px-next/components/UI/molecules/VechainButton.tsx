import PixelboardContract from "blockchain/contracts/pixelboard";
import { waitForTransactionReceipt } from "blockchain/utils/receipts";
import Button from "components/UI/atoms/Button";
import { ReactElement } from "react";
import { useAppSelector } from "store/hooks";
import { setCurrentPrice } from "store/slices/User";
import { ContractMethodParams } from "types/vechain";
import { store } from "store/store";
import Web3 from "web3";

interface VechainButtonProps {
  address: string
}

export default function VechainButton(props: VechainButtonProps): ReactElement {
  const { address } = props
  const { activeId, pickerColor } = useAppSelector((state) => state.pixelControls)
  const { currentPrice } = useAppSelector((state) => state.user)
  const userAddress = useAppSelector((state) => state.user.address)

  if (activeId === undefined) {
    return <Button disabled={true}>BUY</Button>
  }

  const updatePixel = async () => {

    if (pickerColor !== undefined && activeId !== undefined && currentPrice !== undefined) {
      const txComment = "Buy pixel " + activeId + " with color " + pickerColor + "?"
      const methodParams: ContractMethodParams = {
        id: activeId,
        color: Web3.utils.fromAscii(pickerColor.replace('#', '')),
        amount: currentPrice,
        txComment,
        address
      }

      try {
        const response: Connex.Vendor.TxResponse = await PixelboardContract.buyPixel(methodParams)
        const transaction = await waitForTransactionReceipt(response.txid)
        PixelboardContract.getAddrCurrentPrice({ address: address, sender: userAddress }).then(
          (resp: any) => {
            store.dispatch(setCurrentPrice(resp.decoded[0]))
          }
        )
      } catch (e) {
        console.log("Error: " + e)
      }
    }
  }

  return <Button onClick={updatePixel}>BUY</Button>
}
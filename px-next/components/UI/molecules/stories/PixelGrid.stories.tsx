import { Meta, Story } from "@storybook/react";
import Pixel, { PixelProps } from "components/UI/atoms/Pixel";
import PixelGrid from "components/UI/molecules/PixelGrid";
import React, { ComponentProps } from 'react';
import { mockPixelElements } from "storybook/mocks";

export default {
  title: `Molecules/${PixelGrid.name}`,
  decorators: [],
  component: PixelGrid
} as Meta

const Template: Story<ComponentProps<typeof PixelGrid>> = (args) => <PixelGrid {...args} />

export const Filled = Template.bind({});
export const Partial = Template.bind({});

Filled.args = {
  width: 4,
  height: 4,
  resolution: 16,
  children: mockPixelElements(16, false)
}

Partial.args = {
  width: 4,
  height: 4,
  resolution: 16,
  children: mockPixelElements(16, true)
}
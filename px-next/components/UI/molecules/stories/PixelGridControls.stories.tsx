import { Meta, Story } from "@storybook/react";
import React, { ComponentProps } from 'react';
import PixelGridControls from 'components/UI/molecules/PixelGridControls';


const Template: Story<ComponentProps<typeof PixelGridControls>> = (args) => <PixelGridControls {...args} />

export default {
  title: 'Molecules/PixelGridControls',
  decorators: [],
  component: PixelGridControls
} as Meta


export const Normal = Template.bind({});
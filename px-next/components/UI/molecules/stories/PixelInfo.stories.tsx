import { Meta, Story } from "@storybook/react";
import React, { ComponentProps } from 'react';
import PixelInfo from 'components/UI/molecules/ActivePixelInfo';


const Template: Story<ComponentProps<typeof PixelInfo>> = (args) => <PixelInfo {...args} />

export default {
  title: 'Molecules/PixelInfo',
  decorators: [],
  component: PixelInfo
} as Meta


export const Normal = Template.bind({});

Normal.args = {
  pixel: {
    id: 0,
    owner: '0x000',
    color: '#96bc1d',
    amount: 10
  }
}
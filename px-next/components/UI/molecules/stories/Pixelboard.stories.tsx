import { Meta, Story } from "@storybook/react";
import React, { ComponentProps } from 'react';
import { mockPixelboard, mockPixelProps } from "storybook/mocks";
import Pixelboard from 'components/UI/molecules/Pixelboard';


const Template: Story<ComponentProps<typeof Pixelboard>> = (args) => <Pixelboard {...args} />

export default {
  title: 'Molecules/Pixelboard',
  decorators: [],
  component: Pixelboard
} as Meta


export const Empty = Template.bind({})
const emptyBoard =  mockPixelboard("empty", 256)
Empty.args = emptyBoard

export const Partial = Template.bind({})
const partialBoard = mockPixelboard("partial", 256)
partialBoard.pixels = mockPixelProps(256, true)
Partial.args = partialBoard

export const Filled = Template.bind({})
const filledBoard = mockPixelboard("filled", 256)
filledBoard.pixels = mockPixelProps(256, false)
Filled.args = filledBoard
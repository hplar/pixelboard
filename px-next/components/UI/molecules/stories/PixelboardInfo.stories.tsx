import { Meta, Story } from "@storybook/react";
import React, { ComponentProps } from 'react';
import PixelboardInfo from 'components/UI/molecules/PixelboardInfo';


const Template: Story<ComponentProps<typeof PixelboardInfo>> = (args) => <PixelboardInfo {...args} />

export default {
  title: 'Molecules/PixelboardInfo',
  decorators: [],
  component: PixelboardInfo
} as Meta


export const Normal = Template.bind({});

Normal.args = {
    title: 'Sample Board',
    address: '0x000',
    size: 36,
    width: 6,
    height: 6,
    filled: false,
    pixels: [],
    resolution: 16,
    amountSold: 0,
    gap: 0
}
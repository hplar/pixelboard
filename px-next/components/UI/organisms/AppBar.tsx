import FlexBox from 'components/layout/FlexBox';
import dynamic from 'next/dynamic';
import Link from 'next/link';
import { ReactElement } from 'react';
import { createUseStyles } from 'react-jss';


type RuleNames = 'NavLink' | 'NavText'

const useStyles = createUseStyles<RuleNames>({
  NavLink: {
    background: '#333',
    width: '100%',
    '&:hover': {
      background: '#81bccc'
    }
  },
  NavText: {
    flexGrow: 1,
    color: '#fff',
    width: "100%",
    textDecoration: 'none',
  },
})

type NavItem = {
  title: string,
  url: string,
  icon?: string
}

export const AppBar = (): ReactElement => {
  const classes = useStyles()

  const navItems: NavItem[] = [
    {
      title: 'Home',
      url: '/'
    },
    {
      title: 'Dashboard',
      url: '/dashboard'
    },
    {
      title: 'Pixelboards',
      url: '/pixelboards/'
    },
    {
      title: 'About',
      url: '/about',
    },
  ]

  const LoginButton = dynamic(() => import('components/UI/molecules/LoginButton'), {
    ssr: false
  })

  return (
    <>
      <FlexBox>
        <>
          { navItems.map((link, i) => {
            return (
              <div key={i} className={classes.NavLink}>
                <Link href={ link.url }>
                  <a className={ classes.NavText }>{ link.title }</a>
                </Link>
              </div>
            )
          })}
        </>
        <LoginButton />
      </FlexBox>
    </>
  )
}
import FlexBox from "components/layout/FlexBox";
import ColorPicker from "components/UI/atoms/ColorPicker";
import { ReactElement } from "react";
import dynamic from "next/dynamic";

interface VechainControlsProps {
  address: string
}

export default function VechainControls(props: VechainControlsProps): ReactElement {
  const VechainButton = dynamic(() => import('components/UI/molecules/VechainButton'), {
    ssr: false
  })

  return (
  <FlexBox direction={'column'}>
    <ColorPicker />
    <VechainButton address={props.address} />
  </FlexBox>
  )
}
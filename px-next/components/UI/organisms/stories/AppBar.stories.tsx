import React, {ComponentProps} from 'react';
import { Story, Meta } from "@storybook/react";

import { AppBar } from "../AppBar";


export default {
  title: 'Organisms/AppBar',
  decorators: [],
  component: AppBar
} as Meta

const Template: Story<ComponentProps<typeof AppBar>> = (args) => <AppBar />

export const Normal = Template.bind({});
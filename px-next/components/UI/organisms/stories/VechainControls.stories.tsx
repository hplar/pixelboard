import { Meta, Story } from "@storybook/react";
import React, { ComponentProps } from 'react';
import VechainControls from '../VechainControls';


const Template: Story<ComponentProps<typeof VechainControls>> = (args) => <VechainControls {...args} />

export default {
  title: 'Molecules/VechainControls',
  decorators: [],
  component: VechainControls
} as Meta


export const Normal = Template.bind({});
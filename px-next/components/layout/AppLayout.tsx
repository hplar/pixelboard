import { ReactElement } from "react";
import { AppBar } from "components/UI/organisms/AppBar";
import { store } from "store/store";
import { Provider} from "react-redux";

export default function AppLayout(children: any): ReactElement {
  return (
    <>
      <Provider store={store}>
        <AppBar />
        {children.children}
      </Provider>
    </>
  )
}
import { ReactElement } from "react";
import { createUseStyles } from "react-jss";

type RuleNames = 'flexBox'

const useStyles = createUseStyles<RuleNames, FlexBoxStyleProps>({
  flexBox: (props) => ({
    display: 'flex',
    background: props.background ?? "#00000000",
    flexDirection: props.direction,
    justifyContent: props.justifyContent,
    alignItems: props.alignItems,
    height: props.height ? props.height : props.fill ? '100%' : '',
    width: props.fill ? '100%': '',
  })
})

type FlexBoxStyleProps = {
  background?: string
  direction?: 'row' | 'column' | 'row-reverse' | 'column-reverse'
  justifyContent?: 'center' | 'left' | 'right' | 'flex-start' | 'flex-end' | 'space-evenly'
  alignItems?: 'center' | 'left' | 'right' | 'flex-start' | 'flex-end' | 'space-evenly'
  fill?: boolean
  height?: number | string
}

export type FlexBoxProps = FlexBoxStyleProps & {
  children?: ReactElement | ReactElement[]
}

export default function FlexBox(props: FlexBoxProps): ReactElement {
  const children = props

  const classes = useStyles({
    background: props.background,
    direction: props.direction,
    justifyContent: props.justifyContent,
    alignItems: props.alignItems,
    fill: props.fill,
    height: props.height
  })

  return (
    <div className={classes.flexBox}>
      { props.children }
    </div>
  )
}
import { ReactElement } from "react";
import { createUseStyles } from "react-jss";

type RuleNames = "gridArea"

const useStyles = createUseStyles<RuleNames, GridAreaStyleProps>({
  gridArea: (props) => ({
    gridArea: props.area,
    background: props.background,
    justifyContent: props.justifyContent
  })
})

type GridAreaStyleProps = {
  area: string
  background?: string
  justifyContent?: string
}

type GridAreaProps = GridAreaStyleProps & {
  children?: ReactElement | ReactElement[]
}

export default function GridArea(props: GridAreaProps): ReactElement {
  const { children } = props

  const classes = useStyles({
    area: props.area,
    background: props.background,
    justifyContent: props.justifyContent
  })

  return (
    <div className={classes.gridArea}>
      { children }
    </div>
  )
}
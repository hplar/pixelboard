import { FlexBoxProps } from "components/layout/FlexBox";
import { ReactElement } from "react";
import { createUseStyles } from "react-jss";


type NameRules = 'pageGrid'

const useStyles = createUseStyles<NameRules, PageGridStyleProps>({
  pageGrid: (props: PageGridStyleProps) => ({
    display: 'grid',
    width: '100%',
    minWidth: '100%',
    height: '96%',
    minHeight: '96%',
    background: props.background,
    gridTemplateRows: props.templateRows,
    gridTemplateColumns: props.templateColumns,
    gridTemplateAreas: props.template,
    gridGap: props.gridGap,
  })
})

type PageGridStyleProps = {
  template?: string
  templateRows?: string
  templateColumns?: string
  background?: string
  gridGap?: number
}

export type PageGridProps = PageGridStyleProps & {
  children: ReactElement<FlexBoxProps> | ReactElement<FlexBoxProps>[]
}

export default function PageGrid(props: PageGridProps): ReactElement {
  const classes = useStyles({
    template: props.template,
    templateColumns: props.templateColumns,
    templateRows: props.templateRows,
    background: props.background,
    gridGap: props.gridGap
  })

  return (
    <div className={classes.pageGrid}>
      { props.children }
    </div>
  )
}
import { Meta, Story } from "@storybook/react";
import Button from "components/UI/atoms/Button";
import FlexBox from "components/layout/FlexBox";
import React, { ComponentProps } from 'react';

export default {
  title: 'Layout/FlexBox',
  decorators: [],
  component: FlexBox
} as Meta

const Template: Story<ComponentProps<typeof FlexBox>> = (args) => <FlexBox {...args} />

export const SingleItem = Template.bind({});
export const MultipleItems = Template.bind({});

const sharedArgs = {
}

SingleItem.args = {
    background: "#81bccc",
    children: <Button>Sample Button</Button>,
}

MultipleItems.args = {
  background: "#fff",
  children: [
    <Button key={0}>Sample Button</Button>,
    <Button key={1}>Sample Button</Button>,
    <Button key={2}>Sample Button</Button>,
    <Button key={3}>Sample Button</Button>,
    <Button key={4}>Sample Button</Button>,
    <Button key={5}>Sample Button</Button>,
    <Button key={6}>Sample Button</Button>
  ]
}
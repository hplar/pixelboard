import { Meta, Story } from "@storybook/react";
import GridArea from "components/layout/GridArea";
import React, { ComponentProps } from 'react';

export default {
  title: 'Layout/GridArea',
  decorators: [],
  component: GridArea
} as Meta

const Template: Story<ComponentProps<typeof GridArea>> = (
  args
) => <GridArea {...args} />

export const Normal = Template.bind({});

Normal.args = {
  area: 'test',
  background: '#fff',
  children: <div><p>test</p></div>
}
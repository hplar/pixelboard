import { Meta, Story } from "@storybook/react";
import React, { ComponentProps } from 'react';

import AppLayout from "../AppLayout";

export default {
  title: 'Layout/Layout',
  decorators: [],
  component: AppLayout
} as Meta

const Template: Story<ComponentProps<typeof AppLayout>> = (args) => <AppLayout {...args} />
import { Meta, Story } from "@storybook/react";
import FlexBox from "components/layout/FlexBox";
import GridArea from "components/layout/GridArea";
import PageGrid from "components/layout/PageGrid";
import React, { ComponentProps, ReactElement } from 'react';
import { mockPixelboard } from "storybook/mocks";

export default {
  title: 'Layout/PageGrid',
  decorators: [],
  component: PageGrid
} as Meta

const Template: Story<ComponentProps<typeof PageGrid>> = (args) => <PageGrid {...args} />

export const PixelboardDetailPage = Template.bind({});
export const RandomPage = Template.bind({});


PixelboardDetailPage.args = {
  templateRows: '[row1-start] 1fr [row1-end row2-start] 10fr [row2-end row3-start] 1fr [row3-end]',
  templateColumns: '1fr 1fr 1fr 1f',
  template: `
  'left    head     head     right'
  'left    panel    panel    right'
  'left    foot     foot     right' 
  `,
  children: [
    <GridArea key={0} area={'head'} background={'#81bccc'}><p>Header</p></GridArea>,
    <GridArea key={1} area={'left'} background={'#9fccab'}><p>Left</p></GridArea>,
    <GridArea key={2} area={'panel'} background={'#41bccc'}>
      <FlexBox direction={'row'} justifyContent={'center'} alignItems={'center'} fill={true}>
        <Pixelboard {...mockPixelboard("partial", 64)} />
      </FlexBox>
    </GridArea>,
    <GridArea key={3} area={'right'} background={'#cc9f9f'}><p>Right</p></GridArea>,
    <GridArea key={4} area={'foot'} background={'#c29fcc'}><p>Footer</p></GridArea>
  ]
}

RandomPage.args = {
  templateRows: '[row1-start] 1fr [row1-end row2-start] 10fr [row2-end row3-start] 1fr [row3-end row4-start] 1fr [row4-end]',
  templateColumns: '1fr 1fr 1fr 1f 1fr 1fr 1fr',
  template: `
  'head    head    head    right   right   right'
  'left    panel   panel   right   right   right'
  'left    panel   panel   foot    foot    foot'
  'left    panel   panel   foot    foot    foot'
  `,
  children: [
    <GridArea key={0} area={'head'} background={'#81bccc'}><p>Header</p></GridArea>,
    <GridArea key={1} area={'left'} background={'#9fccab'}><p>Left</p></GridArea>,
    <GridArea key={2} area={'panel'} background={'#41bccc'}><p>Panel</p></GridArea>,
    <GridArea key={3} area={'right'} background={'#cc9f9f'}><p>Right</p></GridArea>,
    <GridArea key={4} area={'foot'} background={'#c29fcc'}><p>Footer</p></GridArea>
  ]
}
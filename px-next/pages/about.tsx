import AppLayout from "components/layout/AppLayout";
import FlexBox from "components/layout/FlexBox";
import PageGrid from "components/layout/PageGrid";
import { ReactElement } from "react";

export default function AboutPage(): ReactElement {
  const gridItems = [
    <div key={0}>
      <h3>About</h3>
    </div>
  ]

  return (
    <PageGrid>
      <FlexBox>
        { gridItems }
      </FlexBox>
    </PageGrid>
  )
}

AboutPage.getLayout = function getLayout(page: ReactElement): ReactElement {
  return <AppLayout>{ page }</AppLayout>
}
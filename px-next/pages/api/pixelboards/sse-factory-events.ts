import { ConsumerSubscribeTopic, EachMessagePayload, KafkaMessage } from "kafkajs";
import { NextApiRequest, NextApiResponse } from "next";
import kafka from "kafka/client";
import { Pixelboard } from "types/types";


export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Content-Type', 'text/event-stream;charset=utf-8')
  res.setHeader('Cache-Control', 'no-cache, no-transform')
  res.setHeader('X-Accel-Buffering', 'no')

  const suffix = Math.random().toString(36).slice(2);

  const consumer = kafka.consumer({
    groupId: 'feClient-' + suffix,
    allowAutoTopicCreation: false
  })

  const subscribeTopic: ConsumerSubscribeTopic = {
    topic: 'factory',
    fromBeginning: true,
  }

  try {
    await consumer.connect()
    await consumer.subscribe(subscribeTopic)
    await consumer.run({
      eachMessage: async (messagePayload: EachMessagePayload) => {
        const { topic, partition, message } = messagePayload
        if (message.value !== null) {
          const data = JSON.parse(message.value.toString())
          const pixelboard: Pixelboard = {
            _id: data.id,
            title: data.title,
            address: data.address,
            width: data.width,
            height: data.height,
            size: data.width * data.height
          }
          res.write(`data: ${JSON.stringify(pixelboard)}\n\n`)
        }
      }, autoCommit: false
    })
  } catch (error) {
    console.log('Error: ', error)
  }
}
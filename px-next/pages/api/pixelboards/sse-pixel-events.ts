import kafka from "kafka/client";
import { ConsumerSubscribeTopic, EachMessagePayload } from "kafkajs";
import { NextApiRequest, NextApiResponse } from "next";
import { Pixel, Pixelboard } from "types/types";


export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  const address = req.query.address.toString()

  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Content-Type', 'text/event-stream;charset=utf-8')
  res.setHeader('Cache-Control', 'no-cache, no-transform')
  res.setHeader('X-Accel-Buffering', 'no')

  const suffix = Math.random().toString(36).slice(2);
  const consumer = kafka.consumer({
    groupId: 'feClient-' + suffix,
    allowAutoTopicCreation: false
  })
  const subscribeTopic: ConsumerSubscribeTopic = {
    topic: address,
    fromBeginning: true,
  }

  try {
    await consumer.connect()
    await consumer.subscribe(subscribeTopic)
    await consumer.run({
      eachMessage: async (messagePayload: EachMessagePayload) => {
        const {topic, partition, message} = messagePayload
        if (message.value !== null) {
          const data = JSON.parse(message.value.toString())
          const pixel: Pixel = {
            id: data.index,
            color: data.color,
            owner: data.owner,
            price: data.price,
            isOwned: true
          }
          res.write(`data: ${JSON.stringify(pixel)}\n\n`)
        }
      }, autoCommit: false
    })
  } catch (error) {
    console.log('Error: ', error)
  }
}
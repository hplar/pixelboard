import AppLayout from "components/layout/AppLayout";
import FlexBox from "components/layout/FlexBox";
import PageGrid from "components/layout/PageGrid";
import { ReactElement } from "react";


export default function DashboardPage(): ReactElement {
  const gridItems = [
    <div key={0}>
      <h3>Dashboard</h3>
    </div>
  ]

  return (
    <PageGrid>
      <FlexBox>
        { gridItems }
      </FlexBox>
    </PageGrid>
  )
}

DashboardPage.getLayout = function getLayout(page: ReactElement): ReactElement {
  return <AppLayout>{ page }</AppLayout>
}
import AppLayout from "components/layout/AppLayout";
import FlexBox from "components/layout/FlexBox";
import GridArea from "components/layout/GridArea";
import PageGrid from "components/layout/PageGrid";
import { ReactElement } from "react";

export default function IndexPage(): ReactElement {
  const templateRowHeights = '[row1-start] 1fr [row1-end row2-start] 10fr [row2-end row3-start] 1fr [row3-end]'
  const templateColumWidths = '1fr 1fr 1fr 1fr'
  const gridTemplate = `
    'left    head    head    right'
    'left    panel   panel   right'
    'left    foot    foot    right'   
  `

  return (
    <PageGrid
      template={gridTemplate}
      templateColumns={templateColumWidths}
      templateRows={templateRowHeights}
    >
      <GridArea area={'head'}>
        <FlexBox direction={'row'}><p>Head</p></FlexBox>
      </GridArea>

      <GridArea area={'left'}>
        <FlexBox direction={'column'}><p>Left</p></FlexBox>
      </GridArea>

      <GridArea area={'panel'}>
        <FlexBox direction={'row'}><p>Panel</p></FlexBox>
      </GridArea>

      <GridArea area={'right'}>
        <FlexBox direction={'column'}><p>Right</p></FlexBox>
      </GridArea>

      <GridArea area={'foot'}>
        <FlexBox direction={'row'}><p>Foot</p></FlexBox>
      </GridArea>
    </PageGrid>
  )
}

IndexPage.getLayout = function getLayout(page: ReactElement): ReactElement {
  return <AppLayout>{ page }</AppLayout>
}
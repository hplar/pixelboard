import AppLayout from "components/layout/AppLayout";
import FlexBox from "components/layout/FlexBox";
import GridArea from "components/layout/GridArea";
import PageGrid from "components/layout/PageGrid";
import ActivePixelInfo from "components/UI/molecules/ActivePixelInfo";
import PixelboardInfo from "components/UI/molecules/PixelboardInfo";
import PixelGrid from "components/UI/molecules/PixelGrid";
import PixelGridControls from "components/UI/molecules/PixelGridControls";
import VechainControls from "components/UI/organisms/VechainControls";
import { useRouter } from "next/router";
import { ReactElement, useState } from "react";
import { useAppSelector } from "store/hooks";


export default function PixelboardDetailPage(): ReactElement {
  const router = useRouter()
  const queryId = router.query.id && router.query.id.toString()
  const id = queryId !== undefined ? parseInt(queryId) : 0
  const pixelboard = useAppSelector((state) => state.pixelboards.pixelboards[id])
  const [activePixelId, setActivePixelId] = useState<number>()

  const templateRowHeights = '[row1-start] 1.5fr [row1-end row2-start] 6fr [row2-end row3-start] 0.5fr [row3-end]'
  const templateColumWidths = '1fr 2fr 2fr 1fr'
  const pageGridTemplate = `
    'PBInfo    APInfo     APInfo     VeCtl'
    'PBInfo    PXGrid     PXGrid     VeCtl'
    'PBInfo    PXGCtl     PXGCtl     VeCtl'
  `

  return (
    <PageGrid
      template={pageGridTemplate}
      templateRows={templateRowHeights}
      templateColumns={templateColumWidths}
      gridGap={16}
      background={'#333'}
    >
      <GridArea area={'APInfo'} background={'#fff'}>
        <FlexBox justifyContent={'center'} alignItems={'center'} fill>
          <ActivePixelInfo
            {...pixelboard}
            activePixelId={activePixelId}
          />
        </FlexBox>
      </GridArea>

      <GridArea area={'VeCtl'} background={'#fff'}>
        <FlexBox alignItems={'center'} justifyContent={'center'} fill>
          <VechainControls address={pixelboard.address} />
        </FlexBox>
      </GridArea>

      <GridArea area={'PBInfo'} background={'#fff'}>
        <FlexBox justifyContent={'center'} alignItems={'center'} fill>
          <PixelboardInfo {...pixelboard} />
        </FlexBox>
      </GridArea>

      <GridArea area={'PXGrid'} background={'#fff'}>
        <FlexBox justifyContent={'center'} alignItems={'center'} fill>
          <PixelGrid
            {...pixelboard}
            activePixelId={activePixelId}
            setActivePixelId={setActivePixelId}
          />
        </FlexBox>
      </GridArea>

      <GridArea area={'PXGCtl'} background={'#fff'}>
        <FlexBox direction={'row'} justifyContent={'center'} alignItems={'center'} fill>
          <PixelGridControls />
        </FlexBox>
      </GridArea>

    </PageGrid>
  )
}

PixelboardDetailPage.getLayout = function getLayout(page: ReactElement): ReactElement {
  return <AppLayout>{ page }</AppLayout>
}
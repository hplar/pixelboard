import AppLayout from "components/layout/AppLayout";
import FlexBox from "components/layout/FlexBox";
import PageGrid from "components/layout/PageGrid";
import Link from "next/link";
import { Fragment, ReactElement, useEffect, useState } from "react";
import { createUseStyles } from "react-jss";
import { useAppDispatch, useAppSelector } from "store/hooks";
import { setPixelboard } from "store/slices/Pixelboards";
import { Pixelboard } from "types/types";
import GridArea from "../../components/layout/GridArea";


type RuleName = 'pixelboardCard'

const useStyles = createUseStyles<RuleName>({
  pixelboardCard: {
    padding: 24,
    textDecoration: 'none',
    color: 'black',
    background: '#81bccc99',
    border: "solid 2px #ffffff00",
    transition: ['transform', 'background', 'border', 'color', 'padding'],
    transitionDuration: 400,
    '&:hover': {
      paddingLeft: 16,
      color: "#fff",
      background: "#81bccc",
      border: "solid 2px #81bccc40",
      transition: ['transform', 'background', 'border', 'color', 'padding'],
      transitionDuration: 400,
    }
  }
})

export default function PixelboardListPage(): ReactElement{
  const [ hasEventStream, setHasEventStream] = useState<boolean>(false)
  const dispatch = useAppDispatch()
  const pixelboards = useAppSelector((state) => state.pixelboards.pixelboards)

  const classes = useStyles()

  useEffect(() => {
    if (!hasEventStream) {
      const source = new EventSource(`/api/pixelboards/sse-factory-events`)

      source.onopen = () => {
        setHasEventStream(true)
      }

      source.onmessage = (event) => {
        if (event.data !== null) {
          let pixelboard: Pixelboard = JSON.parse(event.data)
          if (! pixelboards.some(obj => obj._id === pixelboard._id)) {
            dispatch(setPixelboard(pixelboard))
          }
        }
      }

      return () => {
        setHasEventStream(false)
          source.close()
        }
    }
  }, [])

  const templateRowHeights = '[row1-start] 1fr [row1-end row2-start] 10fr [row2-end row3-start] 1fr [row3-end]'
  const templateColumWidths = '1fr 1fr 1fr 1fr'
  const gridTemplate = `
    '. . . .'
    '. p p .'
    '. p p .'
    '. . . .'
  `

  return (
    <PageGrid
      template={gridTemplate}
      templateColumns={templateColumWidths}
      templateRows={templateRowHeights}
    >
      <GridArea area={'p'}>
        <>
          <table>
            <thead>
              <tr>
                <td>Title</td>
                <td>Address</td>
                <td>Status</td>
              </tr>
            </thead>
            <tbody>
              {[...pixelboards].map((pb, i) => {
                return (
                  <tr key={i}>
                    <td>{pb.title}</td>
                    <td>
                      <Link href={`/pixelboards/${pb._id}`}>
                        <a>{pb.address}</a>
                      </Link>
                    </td>
                    <td>{pb.completed ? 'closed' : 'open'}</td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </>
      </GridArea>
    </PageGrid>
  )
}

PixelboardListPage.getLayout = function getLayout(page: ReactElement): ReactElement {
  return (
    <AppLayout>
      {page}
    </AppLayout>
  )
}

const sleep = (delay: number) => new Promise((resolve) => setTimeout(resolve, delay))
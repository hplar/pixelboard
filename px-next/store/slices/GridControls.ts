import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface GridControlsState {
  gap: boolean
  identifiers: boolean
  prices: boolean
  raster: boolean
  resolution: number
}

const initialState: GridControlsState = {
  gap: false,
  identifiers: false,
  prices: false,
  raster: false,
  resolution: 16
}

const gridControlsSlice = createSlice({
  name: 'gridControl',
  initialState,
  reducers: {
    'toggleGap': (state, action: PayloadAction<boolean>) => {
      state.gap = action.payload
    },
    'toggleIdentifiers': (state, action: PayloadAction<boolean>) => {
      state.identifiers = action.payload
    },
    'togglePrices': (state, action: PayloadAction<boolean>) => {
      state.prices = action.payload
    },
    'toggleRaster': (state, action: PayloadAction<boolean>) => {
      state.raster = action.payload
    },
    'setResolution': (state, action: PayloadAction<number>) => {
      state.resolution = action.payload
    },
  }
})

export const { toggleGap, toggleRaster, toggleIdentifiers, togglePrices, setResolution } = gridControlsSlice.actions
export default gridControlsSlice.reducer
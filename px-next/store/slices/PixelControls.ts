import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { action } from "@storybook/addon-actions";

interface PixelControlState {
  activeId: number | undefined
  isOwned: boolean | undefined
  pickerColor: string | undefined
  currentPrice: number
}

const initialState: PixelControlState = {
  activeId: undefined,
  isOwned: undefined,
  pickerColor: undefined,
  currentPrice: 1_000_000_000_000_000_000
}

const infoSlice = createSlice({
  name: 'pixelControl',
  initialState,
  reducers: {
    'setActivePixel': (state, action) => {
      state.activeId = action.payload.id
      state.isOwned = action.payload.isOwned
    },
    'setPickerColor': (state, action: PayloadAction<string>) => {
      state.pickerColor = action.payload
    },
    'setCurrentPrice': (state, action: PayloadAction<number>) => {
      state.currentPrice = action.payload
    },
    'resetCurrentPrice': (state) => {
      state.currentPrice = initialState.currentPrice
    }
  }
})

export const { setActivePixel, setPickerColor, setCurrentPrice, resetCurrentPrice } = infoSlice.actions
export default infoSlice.reducer
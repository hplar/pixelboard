import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { Pixelboard } from 'types/types'


interface PixelboardsState {
  pixelboards: Pixelboard[]
}

const initialState: PixelboardsState= {
  pixelboards: []
}

const pixelboardsSlice = createSlice({
  name: 'pixelboards',
  initialState,
  reducers: {
    'setPixelboard': (state, action) => {
      state.pixelboards.push(action.payload)
    },
    'updatePixelboard': (state, action) => {
      let pixelboard = {...state.pixelboards[action.payload.id]}
      pixelboard.qtyVetByAddr = action.payload.qtyVetByAddr
      pixelboard.qtyVet = action.payload.qtyVet
      pixelboard.qtySold = action.payload.qtySold
      pixelboard.completed = action.payload.completed
      state.pixelboards[action.payload.id] = pixelboard
    }
  },
})

export const { setPixelboard, updatePixelboard } = pixelboardsSlice.actions
export default pixelboardsSlice.reducer
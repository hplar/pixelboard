import { createSlice } from '@reduxjs/toolkit';

import { Pixel } from 'types/types'


interface PixelsState {
  pixels: Pixel[]
}

const initialState: PixelsState = {
  pixels: []
}

const pixelsSlice = createSlice({
  name: 'pixels',
  initialState,
  reducers: {
    'initGrid': (state, action) => {
      state.pixels = action.payload
    },
    'setPixels': (state, action) => {
      state.pixels = action.payload
    },
    'updatePixel': (state, action) => {
      state.pixels[action.payload.id] = action.payload
    },
  }
})

export const { initGrid, setPixels, updatePixel } = pixelsSlice.actions
export default pixelsSlice.reducer
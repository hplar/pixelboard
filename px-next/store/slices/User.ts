import { createSlice, PayloadAction } from '@reduxjs/toolkit';


interface UsersState {
  address: string | undefined
  connected: boolean
  currentPrice: number | undefined
}

const initialState: UsersState= {
  address: undefined,
  connected: false,
  currentPrice: undefined
}

const usersSlice = createSlice({
  name: 'users',
  initialState,
  reducers: {
    'setConnect': (state, action) => {
      state.address = action.payload
      state.connected = true
    },
    'setDisconnect': (state) => {
      state.address = undefined
      state.currentPrice = undefined
      state.connected = false 
    },
    'setCurrentPrice': (state, action) => {
      state.currentPrice = action.payload
    }
  }
})

export const { setConnect, setDisconnect, setCurrentPrice} = usersSlice.actions
export default usersSlice.reducer
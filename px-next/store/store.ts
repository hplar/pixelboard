import { configureStore } from '@reduxjs/toolkit';

import GridControls from "store/slices/GridControls";
import Pixelboards from "store/slices/Pixelboards";
import Pixels from "store/slices/Pixels";
import PixelControls from "store/slices/PixelControls";
import User from './slices/User';

export const store = configureStore({
    reducer: {
        pixelboards: Pixelboards,
        pixels: Pixels,
        gridControls: GridControls,
        pixelControls: PixelControls,
        user: User
    },
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
import FlexBox from "components/layout/FlexBox";
import Button from "components/UI/atoms/Button";
import Pixel, { PixelProps } from "components/UI/atoms/Pixel";
import React, { ReactElement } from "react";


export const mockPixelboard = (type: "empty" | "partial" | "filled", size: number, key?: number): any => {
  const types = {
    empty: {
      address: "0x0000",
      title: "Empty Pixelboard",
      size: size,
      width: Math.sqrt(size),
      height: Math.sqrt(size),
      filled: false,
      pixels: [],
      resolution: 16,
      gap: 0
    },
    partial: {
      address: "0x0000",
      title: "Partially Filled Pixelboard",
      size: size,
      width: Math.sqrt(size),
      height: Math.sqrt(size),
      filled: false,
      pixels: [],
      resolution: 16,
      gap: 0
    },
    filled: {
      address: "0x0000",
      title: "Filled Pixelboard",
      size: size,
      width: Math.sqrt(size),
      height: Math.sqrt(size),
      filled: true,
      pixels: [],
      resolution: 16,
      gap: 0
    }
  }

  return types[type]

}

export const mockPixelElements = (amount: number, randomIds: boolean): ReactElement<PixelProps>[]=> {
  let ids: number[] = []

  if (randomIds) {
    ids = Array.from(
      {length: amount},
      (_, i) => Math.floor(Math.random() * amount)
    )
  } else {
    ids = Array.from({length: amount}, (_, i) => i)
  }

  ids = Array.from(new Set(ids))

  const pixelObjects = Array.from(
    {length: ids.length},
    (_, i) => (
      {
        id: ids[i],
        owner: "0x1",
        color: "#" + (0x1000000 + (Math.random()) * 0xffffff).toString(16).substr(1, 6)
      })
  )

  return Array.from(pixelObjects, (obj, i) => (
    <Pixel
      color={"#" + (0x1000000 + (Math.random()) * 0xffffff).toString(16).substr(1, 6)}
      gridArea={`p${obj.id}`}
      id={i}
      key={i}
    />
  ))
}

export const mockPixelProps = (amount: number, randomIds: boolean): PixelProps[]=> {
  let ids: number[] = []

  if (randomIds) {
    ids = Array.from(
      {length: amount},
      (_, i) => Math.floor(Math.random() * amount)
    )
  } else {
    ids = Array.from({length: amount}, (_, i) => i)
  }

  ids = Array.from(new Set(ids))

  return Array.from(
    {length: ids.length},
    (_, i) => (
      {
        id: ids[i],
        owner: "0x1",
        color: "#" + (0x1000000 + (Math.random()) * 0xffffff).toString(16).substr(1, 6),
        gridArea: `p${ids[i]}`
      })
  )
}



export const mockFlexBox = (
  <FlexBox>
    <Button key={0}>Sample Button</Button>
    <Button key={1}>Sample Button</Button>
    <Button key={2}>Sample Button</Button>
    <Button key={3}>Sample Button</Button>
    <Button key={4}>Sample Button</Button>
    <Button key={5}>Sample Button</Button>
  </FlexBox>
)
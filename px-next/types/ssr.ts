import { Pixel } from "types/types";

export interface PixelboardSSRData {
  _id: number
  title: string
  address: string
  size: number
  height: number
  width: number
  filled: boolean
  template: string
  sold: number
}

export interface PixelboardControlsSSRData {
  resolution: number
  gap: boolean
  raster: boolean
  identifiers: boolean
}

export interface PixelsSSRData {
  pixels: Pixel[]
}
export interface Pixel {
    id: number
    color: string
    owner: string
    price?: number
    isOwned?: boolean
    gridArea?: string
}

export interface Pixelboard {
    _id: number
    address: string
    title: string
    height: number
    width: number
    size: number
    completed?: boolean
    qtyVetByAddr?: number
    qtyVet?: number
    qtySold?: number
}

export type GridAreaStyleProps = {
    gridArea?: string
}

export type PixelboardRgbArray = {
    resolution: number
    arr: any[]
}
export type ContractMethodParams = {
    [key: string]: string | number | null | undefined
}

export type Contract = {
    [key: string]: (params: ContractMethodParams) => any
}
import { Pixel, PixelboardRgbArray } from "types/types";

export function pixelboardToRgb(width: number, resolution: number, pixels: Pixel[]) {
    // let rgbArray: PixelboardRgbArray = {}
    //
    // let row: any[] = [];
    // for (const pixel of store.getState().pixelboard.pixels) {
    //     if (row.length < width) {
    //         row.push(hexToRgb(pixel.color))
    //     }
    //
    //     if (row.length === width) {
    //         rgbArray.arr.push(row)
    //         row = []
    //     }
    // }
    // console.log(rgbArray)
}


function hexToRgb(hex: string) {
    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    if(result){
        let r = parseInt(result[1], 16);
        let g = parseInt(result[2], 16);
        let b = parseInt(result[3], 16);
        return [r, g, b]
    }
    return null;
}